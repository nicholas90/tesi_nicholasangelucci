import dpkt

from Flow import *


class Flows(object):

    def __init__(self, path):
        self.path = path
        self.flows = {}
        self.ipSRC = {}

    def allFlows(self):

        with open(self.path, 'rb') as f:
            pcap = dpkt.pcap.Reader(f)

            for ts, buf in pcap:

                eth = dpkt.ethernet.Ethernet(buf)

                if eth.type != dpkt.ethernet.ETH_TYPE_IP:
                    # print('Non IP Packet type not supported')
                    continue

                ip = eth.data

                if ip.p == 6 or ip.p == 17:
                    tcp = ip.data

                    flow = Flow()
                    flow.src = ip_to_str(ip.src)
                    flow.dst = ip_to_str(ip.dst)
                    flow.sport = tcp.sport
                    flow.dport = tcp.dport

                    key = repr(flow)

                    flow.name = key

                    # if not flows.has_key(key):
                    if key not in self.flows:
                        self.flows[key] = 0
                        #flows_obj[key] = flow

                    self.flows[key] += 1

        f.close()

        return self.flows

    def countDifferentSource(self):

        with open(self.path, 'rb') as f:
            pcap = dpkt.pcap.Reader(f)

            for ts, buf in pcap:

                eth = dpkt.ethernet.Ethernet(buf)

                if eth.type != dpkt.ethernet.ETH_TYPE_IP:
                    # print('Non IP Packet type not supported')
                    continue

                ip = eth.data

                if ip.p == 6 or ip.p == 17:
                    tcp = ip.data

                    # flow = Flow()
                    src = ip_to_str(ip.src)
                    # flow.dst = ip_to_str(ip.dst)
                    # flow.sport = tcp.sport
                    # flow.dport = tcp.dport

                    # key = repr(flow)

                    # flow.name = key

                    # if not flows.has_key(key):
                    if src not in self.ipSRC:
                        self.ipSRC[src] = 0
                        #flows_obj[key] = flow

                    self.ipSRC[src] += 1

        f.close()

        return len(self.ipSRC)