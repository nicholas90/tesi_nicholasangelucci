# def save_pickle():


import shelve


def main():
    file = "save.db"

    f1 = [1, 2, 3]

    f2 = [7, 8, 9]

    features = {
        'features1': f1,
        'features2': f2
    }

    file = "save.db"

    d = shelve.open(file)  # open -- file may get suffix added by low-level
    # library

    d['features1'] = features['features1']  # store data at key (overwrites old data if
    # using an existing key)
    # data = d[key]  # retrieve a COPY of data at key (raise KeyError
    # # if no such key)
    # del d[key]  # delete data stored at key (raises KeyError
    # # if no such key)
    #
    # flag = key in d  # true if the key exists
    # klist = list(d.keys())  # a list of all existing keys (slow!)

    # d['features1'].append(23)
    #
    # # as d was opened WITHOUT writeback=True, beware:
    # d['xx'] = [0, 1, 2]  # this works as expected, but...
    # d['xx'].append(3)  # *this doesn't!* -- d['xx'] is STILL [0, 1, 2]!

    # having opened d without writeback=True, you need to code carefully:
    temp = d['features1']  # extracts the copy
    temp.append(325)  # mutates the copy
    d['features1'] = temp  # stores the copy right back, to persist it

    # or, d=shelve.open(filename,writeback=True) would let you just code
    # d['xx'].append(5) and have it work as expected, BUT it would also
    # consume more memory and make the d.close() operation slower.

    d.close()  # close it

    s = shelve.open(filename=file)
    try:
        print(s['features1'])
    finally:
        s.close()

    # # open the file for writing
    # open_file = open("save.p", 'wb')
    # read_file = open("save.p", "rb")
    # update_file = open('list.pkl', 'w')
    #
    # # this writes the object a to the
    # # file named 'testfile'
    # pickle.dump(features, open_file, pickle.HIGHEST_PROTOCOL)
    #
    # # here we close the fileObject
    # open_file.close()
    #
    # pickle_load = pickle.load(read_file)
    # print(pickle_load)
    # read_file.close()
    #
    # # update dict and write to the file again
    # features.update({'features3': 87})
    # print(features)
    #
    # pickle.dumps(features)
    #
    # pickle_load = pickle.load(read_file)
    # print(pickle_load)
    # read_file.close()


if __name__ == "__main__":
    main()
