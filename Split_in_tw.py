import datetime
import os

import dpkt

import Run_Program
import Utility
from ParsePcap import ParsePcap


class Split_in_tw(ParsePcap):

    #TODO: chiedere domani
    def __init__(self, directory_filePCAP, twTimeDuration):

        super(Split_in_tw, self).__init__(directory_filePCAP)
        self.twTimeDuration = twTimeDuration
        self.pcap_duration = self.get_pcap_duration()

    # restituisce il timestamp del primo pacchetto del pcap
    def get_start_timestamp_PCAP(self):

        # legge ogni pacchetto contenuto nel file .pcap
        with open(self.directory_file_pcap, 'rb') as f:

            pcap = dpkt.pcap.Reader(f)

            # contiene il timestamp del primo pacchetto
            timestamp_inizio = float(min(pcap)[0])

            return datetime.datetime.fromtimestamp(timestamp_inizio)

    # restituisce il timestamp dell'ultimo pacchetto del pcap
    def get_end_timestamp_PCAP(self):

        with open(self.directory_file_pcap, 'rb') as f:

            pcap = dpkt.pcap.Reader(f)

            # contiene il timestamp dell'ultimo pacchetto
            timestamp_fine = float(max(pcap)[0])

            return datetime.datetime.fromtimestamp(timestamp_fine)

    # calcola la durata totale del file pcap
    def get_pcap_duration(self):

        timestamp_inizio = self.get_start_timestamp_PCAP()
        timestamp_fine = self.get_end_timestamp_PCAP()

        return timestamp_fine - timestamp_inizio

    # restituisce il numero di time-windows che è possibile creare da un file.pcap
    def get_number_of_time_windows(self):

        pcap_duration = self.get_pcap_duration()

        # tw_duration: contiene la durata della time-windows
        tw_duration = datetime.timedelta(seconds=self.twTimeDuration)
        # datetime.timedelta(seconds=900) #TODO: modificare con secondi FATTO

        return int(pcap_duration / tw_duration)

    # calcola il tempo finale dell'utlima tw
    def get_timestamp_fine_tw(self, timestamp):

        # tw_duration contiene la durata della time-windows
        tw_duration = datetime.timedelta(seconds=self.twTimeDuration)

        return timestamp + tw_duration

    # splitta il file pcap in time windows, le time windows estratte saranno inserite
    #TODO: finire documentazione
    def split_in_time_windows(self):

        """
        Splitta il file pcap in time windows.
        Le time-windows estratte, saranno inserite nell'apposita cartella relativa al file

        :return: True
        """

        # Utility.remove_all_files_in_directory(self.)

        # contatore di pacchetti
        p = 0

        # conterranno rispettivamente, il timestamp di inizio e fine tw
        timestamp_inizio_tw = self.get_start_timestamp_PCAP()
        timestamp_fine_tw = 0

        directoryFileOutput = ''

        # directoryFolderDestination = ''

        folderTW = str(self.twTimeDuration)

        directoryFolderTW = Utility.get_directory_time_windows() + folderTW + '/'

        # TODO: manca da fare la directory con nome: durata tw [fatto sotto?]
        Utility.check_directory(directoryFolderTW)

        folderPCAP = os.path.basename(os.path.splitext(self.directory_file_pcap)[0])

        directoryFolderDestination = directoryFolderTW + folderPCAP + '/'

        Utility.check_directory(directoryFolderDestination)

        Utility.remove_all_files_in_directory(directoryFolderDestination)

        print('directoryFolderDestination: %s' % directoryFolderDestination)

        # print('directoryFolderDestination: %s' % directoryFolderDestination)

        # Legge ogni pacchetto contenuto nel file .pcap
        with open(self.directory_file_pcap, 'rb') as f:

            # for che itera le time-windows
            for i in range(0, self.get_number_of_time_windows()):

                # TODO: [IMPORTANTE, il file che sto analizzando è grande]
                # TODO: lo faccio fermare alle prime 5 tw (per ora)
                if i < 6:

                    Utility.check_directory(directoryFolderDestination)

                    timestamp_fine_tw = self.get_timestamp_fine_tw(timestamp_inizio_tw)

                    #TODO: chiedere
                    nomeFileOutput = self.generate_name_file_tw(i, str(timestamp_inizio_tw), str(timestamp_fine_tw))

                    directoryFileOutput = directoryFolderDestination + nomeFileOutput

                    Run_Program.run_tshark(self.directory_file_pcap, timestamp_inizio_tw,
                                           timestamp_fine_tw, directoryFileOutput)

                    timestamp_inizio_tw = timestamp_fine_tw

        return True

    def generate_name_file_tw(self, num_tw, timestamp_inizio, timestamp_fine):

        # timestamp_inizio = str(self.get_start_timestamp_PCAP())
        # timestamp_fine = str(self.get_end_timestamp_PCAP())

        data_timestamp_inizio, ora_timestamp_inizio = timestamp_inizio.split(" ")
        ora_inizio, millisecondi_inizio = ora_timestamp_inizio.split(".")
        data_timestamp_fine, ora_timestamp_fine = timestamp_fine.split(" ")
        ora_fine, millisecondi_fine = ora_timestamp_fine.split(".")

        nome_file = "%d_%s_%s.pcap" % (num_tw + 1, ora_inizio, ora_fine)

        return nome_file

