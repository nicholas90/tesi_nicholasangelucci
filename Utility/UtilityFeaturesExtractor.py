import os


def get_directory_graphics():
    """
    Assembla il path della cartella che conterrà le cartelle relative ai file preprocessati.

    :return: il path cartella contenente tutto ciò che riguarda i file pre-processati.
    """

    folder_preprocessed_files = os.getcwd() + '/graphics/'

    return folder_preprocessed_files


# def get_name_file_input(directory_file_input):
#     """
#     A partire dalla directory del file in input, restituisce il nome del file senza l'estenzione .pcap.
#
#     :param directory_file_input: directory dove si trova il file in input da preprocessare.
#     :return: il nome della cartella dove verranno salvate tutte le informzaioni relative al file in input.
#     """
#
#     # ritorna il nome del file in input senza l'estensione
#     return directory_file_input.split("/")[-1].split(".")[0]


def get_directory_file_input(name_file_input):
    # TODO
    """
    Assembla il path completo del file in input.

    :param name_file_input: nome del file in input da preprocessare.
    :return: il path dove è contenuto il file in input da preprocessare
    """

    folder_file_input = get_directory_graphics() + name_file_input + "/"

    return folder_file_input


def get_directory_timewindows_duration(directory_file_input, time_windows_duration):
    # TODO
    """
    Assembla il path relativo alla durata della time-windows e verifica la sua esistenza, se non esiste lo crea.

    Il path conterrà tutto ciò che riguarda quella specifica durata della time-windows
    ed ha la seguente forma: "/graphics/[NOME_FILE_INPUT]/[DURATA_TIME-WINDOWS]/".

    :param directory_file_input: contiene la prima parte del path:
    "/preprocessed/[NOME_FILE_INPUT]/time_windows/.
    :param time_windows_duration: durata della time-windows.
    :return: ritorna il path che contiene tutto ciò che riguarda una determinata time-windows.
    """

    time_windows_duration = str(time_windows_duration)

    path_timewindows_duration = directory_file_input + time_windows_duration + "/"

    return path_timewindows_duration


def generate_directory_save_img(self, f):
    cwd = os.getcwd()

    # contiene la folder nella quale andranno salvati i grafici
    graphics_path = "/graphics"

    text = self.directory_all_tw

    # assemblo il path che seguirà '/graphics'
    for ch in [cwd, '/time_windows']:

        if ch in text:
            text = text.replace(ch, '')

    folder_tw = f.split("_")[0] + '/'

    path = cwd + graphics_path + text + folder_tw

    return path
