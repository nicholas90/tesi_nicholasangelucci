import os


def get_directory_temporary():
    """
    Assembla il path della cartella che conterrà i dataset temporanei.

    :return: il path cartella contenente dataset temporanei.
    """

    folder_temporary_files = os.getcwd() + '/temporary/'

    return folder_temporary_files


def get_directory_dataset():
    """
    Assembla il path della cartella che conterrà i dataset.

    :return: il path cartella contenente i dataset.
    """

    folder_temporary_files = os.getcwd() + '/dataset/'

    return folder_temporary_files
