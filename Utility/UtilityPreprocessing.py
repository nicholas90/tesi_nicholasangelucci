import os
import Utility.Utils as Utils


# def get_directory_pcaps():
#     """
#     Restituisce il path della cartella contenente i file pcap in input.
#
#     :return: path della cartella '/pcaps/'.
#     """
#
#     folder_pcaps = os.getcwd() + '/pcaps/'
#
#     return folder_pcaps


def get_directory_file_input(nome_file_input):
    """
    Assembla il path completo del file in input.

    :param nome_file_input: nome del file in input da preprocessare.
    :return: il path dove è contenuto il file in input da preprocessare
    """

    folder_pcaps = Utils.get_directory_pcaps() + nome_file_input

    return folder_pcaps


def get_name_file_input(directory_file_input):
    """
    A partire dalla directory del file in input, restituisce il nome del file senza l'estenzione .pcap.

    :param directory_file_input: directory dove si trova il file in input da preprocessare.
    :return: il nome della cartella dove verranno salvate tutte le informzaioni relative al file in input.
    """

    # ritorna il nome del file in input senza l'estensione
    return directory_file_input.split("/")[-1].split(".")[0]


def get_directory_preprocessed_files():
    """
    Assembla il path della cartella che conterrà le cartelle relative ai file preprocessati.

    :return: il path cartella contenente tutto ciò che riguarda i file pre-processati.
    """

    folder_preprocessed_files = os.getcwd() + '/preprocessed/'

    return folder_preprocessed_files


def get_directory_all_flows(folder_file_input):
    """
    Assembla il path contenente tutti i flussi per il file in input.

    :param folder_file_input: nome della cartella del file in input.
    :return: il percorso della cartella 'all_flows' relativo ad un determinato file in input
    """

    preprocessed_files_input = get_directory_preprocessed_files() + folder_file_input

    directory_all_flows = preprocessed_files_input + '/all_flows/'

    return directory_all_flows


def get_directory_time_windows(folder_file_input):
    """
    Restituisce il path della cartella 'time-windows' a quale contiene i file splittati in tw.

    :param folder_file_input: nome della cartella per il file in input
    :return: il percorso della cartella 'all_flows' relativo ad un determinato file in input
    """

    preprocessed_files = get_directory_preprocessed_files()

    directory_time_windows = preprocessed_files + folder_file_input + '/time-windows/'

    return directory_time_windows


def get_directory_timewindows_duration(directory_time_windows, time_windows_duration):
    """
    Assembla il path relativo alla durata della time-windows e verifica la sua esistenza, se non esiste lo crea.

    Il path conterrà tutto ciò che riguarda quella specifica durata della time-windows
    ed ha la seguente forma: "/preprocessed/[NOME_FILE_INPUT]/time_windows/[DURATA_TIME-WINDOWS]/".

    :param directory_time_windows: contiene la prima parte del path:
    "/preprocessed/[NOME_FILE_INPUT]/time_windows/.
    :param time_windows_duration: durata della time-windows.
    :return: ritorna il path che contiene tutto ciò che riguarda una determinata time-windows.
    """

    time_windows_duration = str(time_windows_duration)

    path_timewindows_duration = directory_time_windows + time_windows_duration + "/"

    return path_timewindows_duration


def get_directory_preprocessed_timewindows(directory_time_windows_duration, name_timewindows):
    """
    Assembla il path relativo ad ogni time-windows e verifica la sua esistenza; il path è della seguente forma:
    "/preprocessed/[NOME_FILE_INPUT]/time_windows/[DURATA_TIME-WINDOWS]/[TIME-WINDOWS]/"

    :param directory_time_windows_duration: directory contenente il path:
    "/preprocessed/[NOME_FILE_INPUT]/time_windows/[DURATA_TIME-WINDOWS]/".
    :param name_timewindows: nome della time-windows.
    :return: ritorna il path relativo ad una specifica time-windows.
    """

    name_timewindows = str(name_timewindows)

    path_timewindows = directory_time_windows_duration + name_timewindows + "/"

    return path_timewindows


def get_directory_preprocessed_timewindows_flows(directory_preprocessed_timewindows):
    """
    Assembla il path 'flows' e verifica la sua esistenza, se non c'è già lo crea.

    Il path conterrà i flows (quindi le 5-tuple) estratte dal file in input per una determinata durata di time-windows,
    ed ha la seguente forma: "/preprocessed/[NOME_FILE_INPUT]/time_windows/[DURATA_TIME-WINDOWS]/[TIME-WINDOWS]/flows/".

    :param directory_preprocessed_timewindows: directory contenente il path:
    "/preprocessed/[NOME_FILE_INPUT]/time_windows/[DURATA_TIME-WINDOWS]/[TIME-WINDOWS]/flows/".
    :return: path contenente i flussi per una determinata time-windows.
    """

    path_timewindows_flows = directory_preprocessed_timewindows + "flows/"

    return path_timewindows_flows


def get_directory_preprocessed_timewindows_tcp_flows(directory_preprocessed_timewindows):
    """
    Assembla il path 'tcp_flows' e verifica la sua esistenza, se non c'è già lo crea.

    Il path conterrà i flussi TCP per una determinata durata di time-windows, ed ha la seguente forma:
    "/preprocessed/[NOME_FILE_INPUT]/time_windows/[DURATA_TIME-WINDOWS]/[TIME-WINDOWS]/tcp_flows/".

    :param directory_preprocessed_timewindows: directory contenente il path:
    "/preprocessed/[NOME_FILE_INPUT]/time_windows/[DURATA_TIME-WINDOWS]/[TIME-WINDOWS]/tcp_flows/".
    :return: path contenente i flussi TCP per una determinata time-windows.
    """

    path_timewindows_tcp_flows = directory_preprocessed_timewindows + "tcp_flows/"

    return path_timewindows_tcp_flows


def get_directory_preprocessed_timewindows_discarded_tcp_flows(directory_preprocessed_timewindows):
    """
    Assembla il path 'discarded_tcp_flows' e verifica la sua esistenza, se non c'è già lo crea.

    Il path conterrà i flussi non TCP per una determinata durata di time-windows, ed ha la seguente forma:
    "/preprocessed/[NOME_FILE_INPUT]/time_windows/[DURATA_TIME-WINDOWS]/[TIME-WINDOWS]/discarded_tcp_flows/".

    :param directory_preprocessed_timewindows: directory contenente il path:
    "/preprocessed/[NOME_FILE_INPUT]/time_windows/[DURATA_TIME-WINDOWS]/[TIME-WINDOWS]/discarded_tcp_flows/".
    :return: path contenente i flussi non TCP per una determinata time-windows.
    """

    path_timewindows_discarded_tcp_flows = directory_preprocessed_timewindows + "discarded_tcp_flows/"

    return path_timewindows_discarded_tcp_flows


def get_list_of_timewindows(directory_preprocessed_timewindows):
    # lista contenente le time-windows
    time_windows = []

    # crea una lista contenente le time_windows
    for root, dirs, files in os.walk(directory_preprocessed_timewindows):

        level = root.replace(directory_preprocessed_timewindows, '').count(os.sep)

        if level == 0:
            time_windows = dirs
            break

    return time_windows
