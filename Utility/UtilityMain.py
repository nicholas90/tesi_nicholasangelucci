import os


def check_directories():
    """Verifica l'esistenza delle directory necessarie al funzionamento del progetto

    Valuta se tutte le directory necessarie al corretto funzionamento del sistema sono presenti nel progetto.
    Se non lo sono, vengono create.
    L'elenco delle ditectory necesarie si trova nel file "list_directories.txt".
    Una volta verificata l'esistenza ed aver creato le directory mancanti, vengono attribuiti alle cartelle, i permessi
    di lettura e scrittura.
    """

    try:

        # legge la lista di directory contenuta nel file list_directories.txt
        with open('list_directories.txt', 'r') as directories:

            data = directories.read()

            for directory in data.split(","):

                print("directory: ", str(directory))

                if os.path.isdir(directory):
                    os.chmod(directory, 0b111111111)
                else:
                    os.makedirs(directory)
                    os.chmod(directory, 0b111111111)

    # TODO: rivedere gestione eccezione
    except (OSError, IOError) as e:
        print(e)
