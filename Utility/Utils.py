import glob
import os
import shutil


def check_directory(directory):
    """
    Verifica l'esistenza di una directory, se non esiste, la crea.

    :param directory: directory da verificarne l'esistenza.
    """
    try:

        if os.path.isdir(directory):
            os.chmod(directory, 0b111111111)
        else:
            os.makedirs(directory)
            os.chmod(directory, 0b111111111)

    # TODO: rivedere gestione eccezione
    except (OSError, IOError) as e:

        print(e)


def remove_all_files_on_directory(path):
    """
    Controlla se nella cartella discarded_flows sono presenti dei file, se lo sono, li rimuove.

    Perchè se ci sono dei file, non posso fare la move di file con lo stesso nome.

    :param path: path della cartella dalla quale rimuovere i file.
    :return: True
    """

    filelist = [f for f in os.listdir(path) if f.endswith(".pcap")]

    # se ci sono elementi nella directory, li rimuove
    # motivo: se chiami il modulo di preprocessing, significa che tutto quello che avevi fatto grazie a quel modulo,
    # non serve ora
    if filelist:
        for f in filelist:
            os.remove(os.path.join(path, f))

    return True


def remove_all_folder_on_directory(directory):
    """
    Rimuove tutte le cartelle ed i file presenti in una directory

    :param directory: directory dalla quale rimuovere i file.
    """

    directory = directory + "*"

    for sub_directory in glob.glob(directory):
        shutil.rmtree(sub_directory)


def get_name_file_input(directory_file_input):
    """
    A partire dalla directory del file in input, restituisce il nome del file senza l'estenzione .pcap.

    :param directory_file_input: directory dove si trova il file in input da preprocessare.
    :return: il nome della cartella dove verranno salvate tutte le informzaioni relative al file in input.
    """

    # ritorna il nome del file in input senza l'estensione
    return directory_file_input.split("/")[-1].split(".")[0]


def get_directory_pcaps():
    """
    Restituisce il path della cartella contenente i file pcap in input.

    :return: path della cartella '/pcaps/'.
    """

    folder_pcaps = os.getcwd() + '/pcaps/'

    return folder_pcaps
