import os


def get_directory_temporary():
    """
    TODO
    Assembla il path della cartella che conterrà le cartelle relative ai file preprocessati.

    :return: il path cartella contenente tutto ciò che riguarda i file pre-processati.
    """

    folder_temporary_files = os.getcwd() + '/temporary/'

    return folder_temporary_files
