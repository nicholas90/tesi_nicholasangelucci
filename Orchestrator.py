# from scapy.all import *

import sys

import Discretize
import Normalize
import Utility.UtilityPreprocessing as UtilityPreprocessing
import UtilityDB
from FeaturesExtractor import FeaturesExtractor
from LabelAssignment import LabelAssignment
from Preprocessing import Preprocessing
from Switch import Switch

# import UtilityTOTAL as Utility
# from FeaturesExtractor import Features_extractor


"""Classe Orchestrator, si occupa di chiamare i moduli richiesti dall'utente.

Prende in input una lista di tuple (le quali contengono gli argomenti presi da riga di comando), generati mediante la classe Parser, ed in base ai valori
contenuti nelle tuple, chiama il/i modulo/i richiesto/i passandogli i rispettivi parametri.

 chiama i moduli richiesti, in base alle tuple che gli vengono passate.


Attributes:
    likes_spam: A boolean indicating if we like SPAM or not.
    eggs: An integer count of the eggs we have laid.
"""


class Orchestrator(object):

    def __init__(self, tuple_args):
        self.tuple_args = tuple_args

    def __str__(self):
        str(self.tuple_args)

    #TODO: se gli possono essere passati più parametri, va tolto il break. quindi al 99% farò uno alla volta
    def run_orchestrator(self):
        """
        TO DO: gestire -h + uscita
        """
        for t in self.tuple_args:

            v = t[0]

            for case in Switch(v):

                if case('-p'):

                    Orchestrator.p_case(nome_file_input=t[1], tw_time_duration=t[2])

                    break

                if case('-f'):

                    if len(self.tuple_args) == 1:

                        list_of_features = \
                            Orchestrator.f_case(nome_file_input=t[1], tw_time_duration=t[2], graphics=False)

                    elif len(self.tuple_args) > 1:

                        list_of_features = None
                        num_tw = 0

                        args = [i[0] for i in self.tuple_args]

                        if '-graph' in args:

                            pos = args.index('-graph')

                            if self.tuple_args[pos][1] not in ['normal', 'botnet']:
                                sys.exit("[!] WARNING: a -graph devi passare come argomento 'normal' o 'botnet'.")
                            else:
                                list_of_features, num_tw = Orchestrator.f_case(
                                    nome_file_input=t[1], tw_time_duration=t[2], graphics=self.tuple_args[pos][1])

                        if '-l' in args:

                            pos = args.index('-l')  # do something to it

                            if self.tuple_args[pos][1] not in ['normal', 'botnet']:
                                sys.exit("[!] WARNING: a -graph devi passare come argomento 'normal' o 'botnet'.")

                            # TODO: non so se devo gestire l'eccezione select_dataset_compostion is None

                            select_dataset_compostion = \
                                UtilityDB.execute_select_dataset_composition(nome_file=t[1])

                            if select_dataset_compostion:

                                print("Nel Dataset è già presente il file %s, quindi da '%s' le features sono già "
                                      "state estratte e salvate nel Dataset; non puoi salvarle di nuovo." % (
                                      t[1], t[1]))

                                sys.exit(1)

                            else:

                                if list_of_features is None:

                                    print("if - se list_of_features None, quindi se gli ho passato -graph")

                                    # TODO: da testare

                                    list_of_features, num_tw = \
                                        Orchestrator.f_case(nome_file_input=t[1], tw_time_duration=t[2], graphics=False)

                                    code = \
                                        Orchestrator.l_case(features=list_of_features, label=self.tuple_args[pos][1],
                                                            name_dataset_csv=self.tuple_args[pos][2],
                                                            name_label_csv=self.tuple_args[pos][3])

                                    print(code)

                                # se list_of_features non è None, quindi se gli ho passato -graph
                                else:
                                    # se list_of_features non è None
                                    print("else - se list_of_features non è None, quindi gli ho passato -graph")

                                    code = \
                                        Orchestrator.l_case(features=list_of_features, label=self.tuple_args[pos][1],
                                                            name_dataset_csv=self.tuple_args[pos][2],
                                                            name_label_csv=self.tuple_args[pos][3])

                                """Salvataggio del file dal quale sono state estratte le features nel DB."""

                                insert_dataset_compostion = UtilityDB.execute_insert_dataset_composition(
                                    nome_file=t[1], number_tw=num_tw, label=code)

                                if insert_dataset_compostion:
                                    print("La tripla: (%s, %d, %d) è stata inserita nel DB." %
                                          (t[1], num_tw, code))
                                elif insert_dataset_compostion is None:
                                    sys.exit(1)

                    sys.exit(0)

                if case('-r'):

                    self.r_case()

                    break

                if case('-n'):
                    Orchestrator.n_case(csv_dataset=t[1], csv_label=t[2])

                if case('-d'):
                    Orchestrator.d_case(csv_dataset=t[1])

                if case('ten'):
                    print(10)
                    break
                if case('eleven'):
                    print(11)
                    break
                if case():  # default, could also just omit condition or 'if True'
                    print("something else!")
                    # No need to break here, it'll stop anyway
    #
    # def nl_case():
    #     print("t[1]:", t[1])
    #     path_filePCAP = t[1]
    #     # commentato per non far risplittare tutto
    #     # run_splitcap(path_filePCAP)
    #     path_BidirectionalFlow = "flussi_bidirezionali/botnet-capture-20110818-bot-2.pcap/"
    #     print(path_BidirectionalFlow)
    #
    #     # import Create_Dataset
    #     path_fileLabels = t[2]
    #     # Create_Dataset.associate_FlowToLabel(path_filePCAP,path_fileLabels)
    #     print('Pare che ha fatto')

    # -l 'labelled_pcaps/pcaps/botnet-capture-20110818-bot-2.pcap' 'labelled_pcaps/label/capture20110818-2.pcap.netflow.labeled'

    # con -p viene invocato il modulo di preprocessing
    @staticmethod
    def p_case(nome_file_input, tw_time_duration):
        """
        Caso in cui viene chiamato il modulo di preprocessing.
        Il modulo si occupa di preprocessare un file .pcap per una determinata time-windows, quindi, in sequenza:
        - verranno effettuati dei controlli per capire se la coppia (nomefile, tw) è già stata pre-processata (con il
        modulo -p) in precedenza
        -

        :param nome_file_input: nome del  file .pcap che si vuole pre-processare
        :param tw_time_duration: indica di che durata (espressa in secondi) delle time-windows
        :return:
        """

        print("Run preprocessing...")

        print("Preprocessing per la tupla: (%s, %s)" % (nome_file_input, tw_time_duration))

        tw_duration = int(tw_time_duration)

        """
        CONTROLS:
        
        Quando viene chiamato il modulo di preprocessing, viene controllato se:
        - il file in input ha durata >= di 15 minuti, se la durata è < di 15 minuti, il file non viene preprocessato; 
        - è già stata processata la coppia (nomefile, tw), dove nomefile, è il nome del pcap passato in input alla 
        funzione di preprocessing, e tw è la relativa time-windows. Se nel DB è già presente la coppia (nomefile, tw), 
        si chiede all'utente se deve essere rimossa (quindi se il modulo di preprocessing, deve essere chiamato 
        nuovamente per quella coppia), se sì, allora verrà rimosso e si procede al passo successivo; altrimenti 
        l'esecuzione termina. Mentre, se la coppia non è presente, si passa al passo successivo.
        """

        select_preprocessed_file = UtilityDB.execute_select_tuple(nome_file=nome_file_input, tw=tw_duration)

        if select_preprocessed_file:

            b = False

            while not b:

                possible_input = ['yes', 'no']

                value = input("Nel Database è già presente la coppia: (%s, %s), quindi il file '%s' è già stato "
                              "pre-processato una volta;\nse vuoi pre-processarlo di nuovo, digita yes, altrimenti no: "
                              % (nome_file_input, tw_time_duration, nome_file_input))

                print("\n")

                if value not in possible_input:
                    print("Devi digitare 'yes' oppure 'no' per proseguire.")
                    continue

                if value == 'yes':

                    if UtilityDB.execute_delete_tuple(nome_file=nome_file_input, tw=tw_duration):
                        print("La coppia: (%s, %s) è stata rimossa dal DB." % (nome_file_input, tw_time_duration))
                        b = True
                    else:
                        sys.exit(1)

                else:
                    print("Hai deciso di non preprocessare di nuovo la coppia: (%s, %s)."
                          % (nome_file_input, tw_time_duration))
                    print("""End preprocessing.""")
                    sys.exit(0)
        elif select_preprocessed_file is None:
            sys.exit(1)

        path_file_input = UtilityPreprocessing.get_directory_file_input(nome_file_input=nome_file_input)

        preprocessed = Preprocessing(directory_input=path_file_input, tw_duration=tw_duration)

        preprocessed.run_preprocessing()

        insert_preprocessed_file = UtilityDB.execute_insert_tuple(nome_file=nome_file_input, tw=tw_time_duration)

        if insert_preprocessed_file:
            print("La coppia: (%s, %s) è stata inserita nel DB." % (nome_file_input, tw_time_duration))
        elif insert_preprocessed_file is None:
            sys.exit(1)

        sys.exit(0)

    # @staticmethod
    # def l_case(self, path_liv1):
    #
    #     # TODO: mancano i controlli
    #
    #     # files contiene la lista dei file .pcap nel path 'path_liv1', quindi la lista delle time-windows
    #     files_liv1 = [f for f in os.listdir(path_liv1) if f.endswith(".pcap")]
    #
    #     # passo all'oggetto LabelAssignment una lista contenente tutti i file .pcap
    #     la = LabelAssignment()
    #
    #     # per ogni time-windows, generata con -p
    #     for f in files_liv1:
    #         # path della time-windows
    #         path = path_liv1 + f
    #
    #         la.name_file = path
    #
    #         la.download_file()

    @staticmethod
    def f_case(nome_file_input, tw_time_duration, graphics):

        print("Run Features extraction...")

        features_extractor = \
            FeaturesExtractor(name_file_input=nome_file_input, tw_time_duration=tw_time_duration, graphics=graphics)

        list_of_features = features_extractor.run_features_extractor()

        return list_of_features

    @staticmethod
    def l_case(features, label, name_dataset_csv, name_label_csv):

        print("Creation of datasets...")

        label_assignment = LabelAssignment(label=label)

        # dataset contenente le features estratte con l'opzione -f
        dataset = label_assignment.create_dataset(features)

        dataset_l = label_assignment.create_labelled_dataset(size_dataset=len(dataset))

        print("Save on Dataset...")

        # salvo le features nel file 'name_dataset_csv'
        label_assignment.save_csv(name_file=name_dataset_csv, x=dataset)

        # salvo le labels nel file 'name_label_csv'
        label_assignment.save_csv(name_file=name_label_csv, x=dataset_l)

        return label_assignment.code

        # """IMPORTANTE"""
        #
        #
        #
        #
        # exit(0)
        #
        #
        #
        # import Normalize
        #
        # dataset_n = Normalize.normalize_solam(dataset, name_file)
        #
        # return True
        #
        # exit(0)
        #
        # dat = label_assignment.create_dataset(features, code)
        #
        # label_assignment.save_numpy_array(name_file="array.out", x=dat)
        #
        # exit(0)
        #
        # import numpy as np
        #
        # # all_raw = np.array([][], dtype='d')
        # #
        # # print("prima del for")
        #
        # all_raw = list()
        #
        # for i in range(0, 8):
        #     sub_dat = dat[:, [i]].astype(int)
        #
        #     # ritorna: hist (ossia: i dati per la costruzione dell'istogramma) e bins (le categorie per i dati)
        #     hist, bins = label_assignment.discretize(sub_dat, 7)
        #
        #     discretized_value = label_assignment.get_discretized_value(arr=sub_dat, bins=bins)
        #
        #     all_raw.append(discretized_value)
        #
        # # pprint(np.asarray(all_raw))
        #
        # dat_seven_bins = np.asarray(all_raw)
        #
        # label_assignment.write_to_csv(array=dat_seven_bins, csv_name="dataset/dataset_7bins_botnet.csv")
        #
        # exit(0)
        #
        # dat2 = label_assignment.create_dataset_nn(features, code)
        #
        # pprint(dat2)
        #
        # label_assignment.write_to_csv(array=dat2, csv_name="dataset/dataset2.csv")
        #
        # exit(0)

    @staticmethod
    def n_case(csv_dataset, csv_label):

        # dataset normalizzato, in formato DataFrame
        normalized_dataset = Normalize.get_normalized_dataframe(file_csv=csv_dataset)

        # concateno alle label
        labelled_dataset = Normalize.get_labelled_dataframe(file_csv=csv_label)

        normalized_labelled_dataset = \
            Normalize.get_normalized_labelled_dataset(normalized_df=normalized_dataset, labelled_df=labelled_dataset)

        Normalize.write_csv(dataframe=normalized_labelled_dataset)

        exit(0)

        label = Normalize.get_label(dataset)

        prova_normalized = Normalize.add_label_to_normalize_dataset(normalized_dataset=prova, label=label)

        label_assignment = LabelAssignment(label='b')  # non conta, solo per chiamare il write

        label_assignment.write_to_csv(array=prova_normalized, csv_name=normalized_dataset)

        exit(0)

    @staticmethod
    def d_case(csv_dataset):

        # dataset, in formato DataFrame
        dataset = Discretize.get_dataframe(file_csv=csv_dataset)

        # Normalize.write_csv(dataframe=normalized_labelled_dataset)

        normal_label_dataset, botnet_label_dataset = Discretize.split_dataframe(df=dataset)

        normal_dataset = Discretize.remove_label_column_dataframe(df=normal_label_dataset)

        botnet_dataset = Discretize.remove_label_column_dataframe(df=botnet_label_dataset)

        seven_bins_normal_dataset = Discretize.dataset_bins(dataset=normal_dataset, number_of_bins=7)

        seven_bins_botnet_dataset = Discretize.dataset_bins(dataset=botnet_dataset, number_of_bins=7)

        Discretize.write_to_csv(array=seven_bins_normal_dataset, csv_name="dataset/dataset_7bins_normal.csv")

        Discretize.write_to_csv(array=seven_bins_botnet_dataset, csv_name="dataset/dataset_7bins_botnet.csv")

        twenty_bins_normal_dataset = Discretize.dataset_bins(dataset=normal_dataset, number_of_bins=20)

        twenty_bins_botnet_dataset = Discretize.dataset_bins(dataset=botnet_dataset, number_of_bins=20)

        Discretize.write_to_csv(array=twenty_bins_normal_dataset, csv_name="dataset/dataset_20bins_normal.csv")

        Discretize.write_to_csv(array=twenty_bins_botnet_dataset, csv_name="dataset/dataset_20bins_botnet.csv")

        exit(0)

        # print(normal_dataset, botnet_dataset)

        Discretize.write_to_csv(array=dat_seven_bins, csv_name="dataset/dataset_7bins_botnet.csv")

        exit(0)

        import numpy as np

        # all_raw = np.array([][], dtype='d')
        #
        # print("prima del for")

        all_raw = list()

        n

        for i in range(0, 8):
            sub_dat = dat[:, [i]].astype(int)

            # ritorna: hist (ossia: i dati per la costruzione dell'istogramma) e bins (le categorie per i dati)
            hist, bins = label_assignment.discretize(sub_dat, 7)

            discretized_value = label_assignment.get_discretized_value(arr=sub_dat, bins=bins)

            all_raw.append(discretized_value)

        # pprint(np.asarray(all_raw))

        dat_seven_bins = np.asarray(all_raw)

        label_assignment.write_to_csv(array=dat_seven_bins, csv_name="dataset/dataset_7bins_botnet.csv")

        exit(0)

        label = Normalize.get_label(dataset)

        prova_normalized = Normalize.add_label_to_normalize_dataset(normalized_dataset=prova, label=label)

        label_assignment = LabelAssignment(label='b')  # non conta, solo per chiamare il write

        label_assignment.write_to_csv(array=prova_normalized, csv_name=normalized_dataset)

        exit(0)

    @staticmethod
    def r_case():

        # rimuove il database (database.db) utilizzato di default nel progetto
        UtilityDB.remove_database()

        print("Il database è stato eliminato correttamente dal progetto")

        sys.exit(0)
