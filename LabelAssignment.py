import os

import numpy as np
from sklearn import preprocessing

import Utility.UtilityLabelAssignment as UtilityLabelAssignment

"""Classe LabelAssignment, dato un dizionario di features, si occupa attribuirgli una label.

Inoltre mette a disposizione i metodi per la scrittura (su file .csv) del dataset e delle rispettive label; 
i file scritti sono due, uno contenente i valori delle features ed uno contenente le label. 

Attributes:
    label: A boolean indicating if we like SPAM or not.
"""


class LabelAssignment:

    def __init__(self, label):

        self.label = label
        self.code = self.get_code()

        # coppie (nomefileTW, label)

    def get_code(self):
        """
        Genera la label.

        Se il traffico è di tipo normale, la label sarà 0, altrimenti 1.

        :return: int (0 oppure 1).
        """

        # genero il codice delle label

        # setto la giusta label
        if self.label == 'normal':
            code = 0
        else:
            code = 1

        return code

    def assign_label(self, dataset, code):

        dataset_labelled = np.insert(dataset, 8, code, axis=1)

        return dataset_labelled

    # creo il dataset
    def create_dataset(self, features):
        """
        Crea il dataset (in formato numpy array).

        :param features: lista contenente le features.
        :return: numpy array contenente le features, formattate correttamente per la creazione del dataset
        """

        features1 = np.array(features[0], dtype='f')
        features2 = np.array(features[1], dtype='f')
        features3 = np.array(features[2], dtype='f')
        features4 = np.array(features[3], dtype='f')
        features5 = np.array(features[4], dtype='f')
        features6 = np.array(features[5], dtype='f')
        features7 = np.array(features[6], dtype='f')
        features8 = np.array(features[7], dtype='f')

        a = np.array(features1)
        b = np.array(features2)
        c = np.array(features3)
        d = np.array(features4)
        e = np.array(features5)
        f = np.array(features6)
        g = np.array(features7)
        h = np.array(features8)

        dataset = np.column_stack((a, b, c, d, e, f, g, h))

        return dataset

    def create_labelled_dataset(self, size_dataset):
        """
        Crea un numpy array contenente le label.
        L'array avrà dimensione 'size_dataset' e sarà formato da 'code'.

        :param size_dataset: grandezza del dataset non labellato, in numero di righe
        :return: numpy array che rappresenta il dataset labellato.
        """

        labels_dataset = None

        # numpy array contenente le labels (normal)
        if self.code == 0:
            labels_dataset = np.zeros((size_dataset, 1), dtype=np.int)
        elif self.code == 1:
            labels_dataset = np.ones((size_dataset, 1), dtype=np.int)

        return labels_dataset

    def save_csv(self, name_file, x):
        """
        Salva un numpy array in un file csv.

        Se il file esiste già, allora lo carico, lo modifico e lo sovrascrivo, altrimenti lo creo e vi salvo il dataset.
        In questo caso il numpy array è correttamente formattato nel formato dataset.

        :param name_file: nome file csv.
        :param x: numpy array da salvare
        :return: True
        """

        # TODO: eccezione?? ed il return è giusto?

        # se il file non esiste lo creo

        path_file = UtilityLabelAssignment.get_directory_temporary() + name_file

        # se il file esiste già, allora lo carico, lo modifico e lo sovrascrivo
        if os.path.isfile(path_file):
            # file exists
            with open(path_file, 'ab+') as file:
                np.savetxt(file, x, delimiter=",")
        # altrimenti lo creo e vi salvo sopra il dataset.
        else:

            # TODO: in questo caso devo pulire il contenuto della tabella.. se no si fa casino.. oppure faccio con il -r

            np.savetxt(path_file, x, delimiter=',')

        return True

    def create_dataset_nn(self, features, code):

        features1 = np.array(features[0], dtype='f')
        features2 = np.array(features[1], dtype='f')
        features3 = np.array(features[2], dtype='f')
        features4 = np.array(features[3], dtype='f')
        features5 = np.array(features[4], dtype='f')
        features6 = np.array(features[5], dtype='f')
        features7 = np.array(features[6], dtype='f')
        features8 = np.array(features[7], dtype='f')

        # quanti valori ci sono per ogni colonna
        labels_size = [code] * len(features[0])

        # print(len(labels))

        labels = np.array([], dtype='f')

        # numpy array contenente le labels (botnet)
        if code == 0:
            labels = np.zeros_like(labels_size, dtype=np.int)
        elif code == 1:
            labels = np.ones_like(labels_size, dtype=np.int)

        a = np.array(features1)
        b = np.array(features2)
        c = np.array(features3)
        d = np.array(features4)
        e = np.array(features5)
        f = np.array(features6)
        g = np.array(features7)
        h = np.array(features8)

        dataset = np.column_stack((a, b, c, d, e, f, g, h))

        normalized = preprocessing.MinMaxScaler()

        minmax_normalized = normalized.fit_transform(dataset)

        # minmax_normalized_labelled = np.insert(minmax_normalized, 8, code, axis=1)

        # return minmax_normalized_labelled

    def write_to_csv(self, csv_name, array):

        # np.savetxt(csv_name, array, delimiter=",")

        with open(csv_name, 'ab+') as file:
            np.savetxt(file, array, delimiter=",")

    def discretize(self, data, bins):

        # return np.histogram(a=data, bins=bins)

        discrete = np.array([], dtype='f')

        discrete, bins = np.histogram(data, bins, density=False)

        # np.append(discrete, [np.digitize(data, bins) - 1], axis=0)

        return discrete, bins

        # split = np.array_split(np.sort(data), bins)
        #
        # cutoffs = [x[-1] for x in split]
        # cutoffs = cutoffs[:-1]
        #
        # discrete = np.digitize(data, cutoffs, right=True)
        #
        # return discrete, cutoffs

    def get_discretized_value(self, arr, bins):

        inds = np.digitize(arr, bins, right=True)

        for c in range(len(inds)):

            # nel caso in cui il valore appartenga al limite inferiore, lo aggrego alla prima categoria
            if inds[c][0] == 0:
                inds[c] = 1

        return inds.ravel()
