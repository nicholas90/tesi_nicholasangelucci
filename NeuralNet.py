import matplotlib.pyplot as plt
import numpy as np
from keras import backend
from keras.layers import Dense
from keras.models import Sequential
from keras.utils.vis_utils import plot_model
from sklearn import metrics
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import train_test_split


# Plot a confusion matrix.
# cm is the confusion matrix, names are the names of the classes.
def plot_confusion_matrix(cm, names, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(names))
    plt.xticks(tick_marks, names, rotation=45)
    plt.yticks(tick_marks, names)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


# Plot an ROC. pred - the predictions, y - the expected output.
def plot_roc(pred, y):
    fpr, tpr, _ = roc_curve(y, pred)
    roc_auc = auc(fpr, tpr)

    plt.figure()
    plt.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic (ROC)')
    plt.legend(loc="lower right")
    plt.show()

print(backend.backend())

# load dataset
dataset = np.loadtxt(fname="dataset/dataset.csv", delimiter=",")

print("das")

# splitto in variabili di input ed output
X = dataset[:, 0:8].astype(float)
Y = dataset[:, 8]

# print(len(X))
# print(X)
# print(Y)

# split into 67% for train and 33% for test
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.33, shuffle=True)

print(len(X_train))
print(len(y_train))
print(len(X_test))
print(len(y_test))

# Create model 1
model1 = Sequential()
model1.add(Dense(60, input_dim=8, activation="relu", kernel_initializer="uniform"))
model1.add(Dense(45, activation="relu", kernel_initializer="uniform"))
model1.add(Dense(10, activation="relu", kernel_initializer="uniform"))
model1.add(Dense(1, activation="sigmoid", kernel_initializer="uniform"))

# Create model 2
model2 = Sequential()
model2.add(Dense(60, input_dim=8, activation="tanh", kernel_initializer="uniform"))
model2.add(Dense(45, activation="tanh", kernel_initializer="uniform"))
model2.add(Dense(10, activation="tanh", kernel_initializer="uniform"))
model2.add(Dense(1, activation="sigmoid", kernel_initializer="uniform"))

# Compile model
model1.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])
model2.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])

# Fit the model
history1 = model1.fit(X_train, y_train, validation_split=0.33, validation_data=(X_test, y_test), epochs=600,
                      batch_size=10, verbose=2, shuffle=True)
history2 = model2.fit(X_train, y_train, validation_split=0.33, validation_data=(X_test, y_test), epochs=600,
                      batch_size=10, verbose=2, shuffle=True)

# # summarize history for accuracy
# plt.plot(history1.history['acc'])
# plt.plot(history1.history['val_acc'])
# # plt.plot(history2.history['acc'], 'r-')
# # plt.plot(history2.history['val_acc'], 'g-')
# plt.title('model accuracy')
# plt.ylabel('accuracy')
# plt.xlabel('epoch')
# # plt.legend(['train', 'test'], loc='upper left')
# plt.legend(['train', 'test'], loc='lower right')
#
# plt.show()
#
# exit(0)

# list all data in history

print(history1.history.keys())
print(history2.history.keys())

# summarize history for accuracy
plt.plot(history1.history['acc'])
plt.plot(history1.history['val_acc'])
plt.plot(history2.history['acc'], 'r-')
plt.plot(history2.history['val_acc'], 'g-')
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
# plt.legend(['train', 'test'], loc='upper left')
plt.legend(['train model 1', 'test model 1', 'train model 2', 'test model 2'], loc='lower right')

nome_png = "model_accuracy"

figura = "graphics/" + nome_png + ".png"

plt.savefig(fname=figura, dpi='figure', format='png')

plt.show()

# summarize history for loss
plt.plot(history1.history['loss'])
plt.plot(history1.history['val_loss'], label='Network 2 Val Loss')
plt.plot(history2.history['loss'], 'r-')
plt.plot(history2.history['val_loss'], 'g-')
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
# plt.legend(['train', 'test'], loc='upper left')
plt.legend(['train model 1', 'test model 1', 'train model 2', 'test model 2'], loc='upper right')

nome_png = "model_loss"

figura = "graphics/" + nome_png + ".png"

plt.savefig(fname=figura, dpi='figure', format='png')

plt.show()

# # Predicting the Test set results
# y_pred = model.predict(X_test)
# y_pred = (y_pred > 0.5)

# evaluate the model (stampa l'accuratezza finale del modello)
scores = model1.evaluate(X_test, y_test)
print(scores)
print("%s: %.2f%%" % (model1.metrics_names[1], scores[1] * 100))

scores = model2.evaluate(X_test, y_test)
print(scores)
print("%s: %.2f%%" % (model2.metrics_names[1], scores[1] * 100))

# # calculate predictions
# predictions = model.predict(X)
#
# # round predictions
# rounded = [round(x[0]) for x in predictions]
# print(rounded)

# note in kera model.predict() will return predict probabilities
pred_prob1 = model1.predict(X_test, verbose=0)
pred_prob2 = model2.predict(X_test, verbose=0)

# preds = clf.predict_proba(Xtest)[:,1]
fpr1, tpr1, threshold1 = metrics.roc_curve(y_test, pred_prob1)
fpr2, tpr2, threshold2 = metrics.roc_curve(y_test, pred_prob2)
roc_auc1 = metrics.auc(fpr1, tpr1)
roc_auc2 = metrics.auc(fpr2, tpr2)

plt.title('Receiver Operating Characteristic')
plt.plot(fpr1, tpr1, 'b', label='AUC = %0.2f' % roc_auc1)
plt.plot(fpr2, tpr2, 'g', label='AUC = %0.2f' % roc_auc2)
plt.legend(loc='lower right')
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')

nome_png = "roc"

figura = "graphics/" + nome_png + ".png"

plt.savefig(fname=figura, dpi='figure', format='png')

plt.show()

plot_model(model1, to_file='model1.png')
plot_model(model2, to_file='model2.png')

exit(0)
import numpy as np

from sklearn.metrics import confusion_matrix

from sklearn import metrics

# Measure accuracy
pred = model.predict(X_test)
pred = np.argmax(pred)
y_compare = np.argmax(y_test)
print(y_compare)
score = metrics.accuracy_score(tpr, pred_prob)
print("Final accuracy: {}".format(score))

# Compute confusion matrix
cm = confusion_matrix(y_compare, pred_prob)
np.set_printoptions(precision=2)
print('Confusion matrix, without normalization')
print(cm)
plt.figure()
plot_confusion_matrix(cm, diagnosis)

# Normalize the confusion matrix by row (i.e by the number of samples
# in each class)
cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
print('Normalized confusion matrix')
print(cm_normalized)
plt.figure()
plot_confusion_matrix(cm_normalized, diagnosis, title='Normalized confusion matrix')

plt.show()
