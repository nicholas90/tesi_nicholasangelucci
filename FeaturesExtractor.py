import math
import os
from collections import OrderedDict

import dpkt
import numpy as np

import Graphics
import Utility.UtilityFeaturesExtractor as UtilityFeaturesExtractor
import Utility.UtilityPreprocessing as UtilityPreprocessing
import Utility.Utils as Utils
import UtilityTOTAL as Utility
import Utility_PCAP
import print_packets

"""Classe FeaturesExtractor, si occupa generare le rappresentazioni.

Dato il path contenenti le time-windows, per ogni file genera la relativa rappresentazione, 
una volta generata, [viene stampato un grafico d'esempio] e la rappresentazione viene salvata in un 
file .csv

Attributes:
    likes_spam: A boolean indicating if we like SPAM or not.
    eggs: An integer count of the eggs we have laid.
"""


class FeaturesExtractor(object):

    dataset = """dataset.csv"""

    def __init__(self, name_file_input, tw_time_duration, graphics=False):

        self.name_file_input = name_file_input
        self.tw_time_duration = tw_time_duration
        self.graphics = graphics
        self.directory_graphics = self.get_directory_graphics()
        self.directory_all_tw = self.get_directory_all_tw()

    def get_directory_all_tw(self):
        folder_name = Utils.get_name_file_input(directory_file_input=self.name_file_input)

        directory_time_windows = UtilityPreprocessing.get_directory_time_windows(folder_file_input=folder_name)

        directory_timewindows_duration = UtilityPreprocessing.get_directory_timewindows_duration(
            directory_time_windows=directory_time_windows, time_windows_duration=self.tw_time_duration)

        return directory_timewindows_duration

    def get_directory_graphics(self):

        folder_name = Utils.get_name_file_input(directory_file_input=self.name_file_input)

        directory_file_input = UtilityFeaturesExtractor.get_directory_file_input(name_file_input=folder_name)

        directory_graphics = UtilityFeaturesExtractor.get_directory_timewindows_duration(
            directory_file_input=directory_file_input, time_windows_duration=self.tw_time_duration)

        return directory_graphics

    # chiama i metodi per generare la rappresentazione e *salva le rappresentazioni create in un file .csv
    def run_features_extractor(self):

        # TODO: implementare la richiesta: vuoi salvare nel dataset le rappresentazioni che stai per estrarre?
        # no.. lo fa l'opzione -w

        directory_all_tw = self.directory_all_tw

        # lista contenente le time-windows
        list_tw = Utility.get_list_of_timewindows(directory_preprocessed_timewindows=directory_all_tw)

        list_tw = list(map(int, list_tw))

        asd = len(list_tw)

        print(list_tw)
        print("asd: %d" % asd)

        list_tw.sort()

        c = 0

        # file = "save.db"
        #
        # os.remove(file)

        list_features = list()

        # per ogni time-windows nel path 'directory_all_tw'
        # TODO: dividere il for in fiunzioni
        for name in list_tw:

            print(name)

            # path delle time-windows che sto pre-processando
            directory_preprocessed_timewindows = Utility.get_directory_preprocessed_timewindows(
                directory_time_windows_duration=directory_all_tw, name_timewindows=name)

            print(directory_preprocessed_timewindows)

            # TODO: documentare
            directory_flows = Utility.get_directory_preprocessed_timewindows_flows(
                directory_preprocessed_timewindows=directory_preprocessed_timewindows)

            num_flows = len([f for f in os.listdir(directory_flows)
                             if os.path.isfile(os.path.join(directory_flows, f)) and f[0] != '.'])

            print("Numero di flussi: %d" % num_flows)

            # get_directory_preprocessed_timewindows_flows [TODO]
            directory_tcp_flows = Utility.get_directory_preprocessed_timewindows_tcp_flows(
                directory_preprocessed_timewindows=directory_preprocessed_timewindows)

            num_tcp_flows = len([f for f in os.listdir(directory_tcp_flows)
                                 if os.path.isfile(os.path.join(directory_tcp_flows, f)) and f[0] != '.'])

            print("Numero di flussi TCP: %d" % num_tcp_flows)

            # TODO: conteollare se va bene o si puo migliorare. ->
            # TODO: facendo così si risparmiua tutta la computazione presente nell'else
            # se non ci sono flussi tcp nella directory 'directory_tcp_flows' non posso calcolare le features,
            # quindi, assumeranno tutte valore 0
            if num_tcp_flows == 0:

                features = {
                    'features1': float(0),
                    'features2': float(0),
                    'features3': float(0),
                    'features4': float(0),
                    'features5': float(0),
                    'features6': float(0),
                    'features7': float(0),
                    'features8': float(0),
                }

            else:
                """passo 1 mi estraggo tutto ciò che serve per calcolare le features"""

                # flowset conterrà i [finire] flowset che devono essere  con un numero di flussi entranti >= di 3
                flowset = self.get_flowset(path=directory_tcp_flows)

                # estrae a partire dal flowset, le caratteristiche che servono per il calcolo delle features 1 e 2,
                # però sotto forma di rappresentazioni e numeri
                # TODO: da vedere se separare in due funzioni
                features_1_2 = self.flowset_extract_traffic_characterization(path=directory_tcp_flows,
                                                                             flow_counter=flowset)

                # TODO: automatizzare => lista = dict.tolist()
                path = self.get_directory_graphics() + str(c)

                if self.graphics:

                    # se features1 contiene elementi, genero il grafico
                    if features_1_2[2].tolist():
                        Graphics.get_graph(path, features_1_2[2].tolist(), "features1",
                                           "Entropy of Packet Counts for each Flow Set",
                                           xlabel="IP address of Flow Set", ylabel="Entropy of Packet Count")

                    # se features2 contiene elementi, genero il grafico
                    if features_1_2[3].tolist():
                        Graphics.get_graph(path, features_1_2[3].tolist(), "features2",
                                           "Entropy of time gap for each Flow Set",
                                           xlabel="IP address of Flow Set", ylabel="Entropy of Time Gap")

                features_3_to_8 = self.get_tcp_features(path_flows=directory_flows, path_tcp_flows=directory_tcp_flows)

                features = {
                    'features1': features_1_2[0],
                    'features2': features_1_2[1],
                    'features3': features_3_to_8['features_3'],
                    'features4': features_3_to_8['features_4'],
                    'features5': features_3_to_8['features_5'],
                    'features6': features_3_to_8['features_6'],
                    'features7': features_3_to_8['features_7'],
                    'features8': features_3_to_8['features_8'],
                }

            list_features.append(features)

            c += 1

            # TODO: documentare
            directory_discarded_tcp_flows = Utility.get_directory_preprocessed_timewindows_discarded_tcp_flows(
                directory_preprocessed_timewindows=directory_preprocessed_timewindows)

        features1_list = [dicto['features1'] for dicto in list_features if 'features1' in dicto.keys()]

        features2_list = [dicto['features2'] for dicto in list_features if 'features2' in dicto.keys()]

        features3_list = [dicto['features3'] for dicto in list_features if 'features3' in dicto.keys()]

        features4_list = [dicto['features4'] for dicto in list_features if 'features4' in dicto.keys()]

        features5_list = [dicto['features5'] for dicto in list_features if 'features5' in dicto.keys()]

        features6_list = [dicto['features6'] for dicto in list_features if 'features6' in dicto.keys()]

        features7_list = [dicto['features7'] for dicto in list_features if 'features7' in dicto.keys()]

        features8_list = [dicto['features8'] for dicto in list_features if 'features8' in dicto.keys()]

        if self.graphics:
            path_timewindows_duration = self.get_directory_graphics()

            Graphics.get_graph(path_timewindows_duration, features1_list, "features1",
                               "Entropy of Packet Counts (per time window)",
                               xlabel="Time-windows", ylabel="Entropy of Packet Counts", color=self.graphics)

            Graphics.get_graph(path_timewindows_duration, features2_list, "features2",
                               "Entropy of time gap (per time window)",
                               xlabel="Time-windows", ylabel="Entropy of time gap", color=self.graphics)

            Graphics.get_graph(path_timewindows_duration, features3_list, "features3",
                               "Ratio of one-way TCP connections (per time window)",
                               xlabel="Time-windows", ylabel="Ratio of one-way TCP connections",
                               color=self.graphics)

            Graphics.get_graph(path_timewindows_duration, features4_list, "features4",
                               "Ratio of incoming to outgoing TCP packets (per time window)",
                               xlabel="Time-windows", ylabel="Ratio of incoming to outgoing TCP packets",
                               color=self.graphics)

            Graphics.get_graph(path_timewindows_duration, features5_list, "features5",
                               "Ratio of TCP packets to overall packets (per time window)",
                               xlabel="Time-windows", ylabel="Ratio of TCP packets to overall packets",
                               color=self.graphics)

            Graphics.get_graph(path_timewindows_duration, features6_list, "features6",
                               "Number of SYN flags (per time window)",
                               xlabel="Time-windows", ylabel="Number of SYN flags", color=self.graphics)

            Graphics.get_graph(path_timewindows_duration, features7_list, "features7",
                               "Number of FIN flags (per time window)",
                               xlabel="Time-windows", ylabel="Number of FIN flags", color=self.graphics)

            Graphics.get_graph(path_timewindows_duration, features8_list, "features8",
                               "Number of PSH flags (per time window)",
                               xlabel="Time-windows", ylabel="Number of PSH flags", color=self.graphics)

        all_features = list()

        all_features.append(features1_list)
        all_features.append(features2_list)
        all_features.append(features3_list)
        all_features.append(features4_list)
        all_features.append(features5_list)
        all_features.append(features6_list)
        all_features.append(features7_list)
        all_features.append(features8_list)

        return all_features, c

    """FEATURES EXTRACTION"""

    # features1: Entropy of packet count for similar flows
    @staticmethod
    def get_entropy_of_packet_counts_foreach_flowset(flowset):

        print('Calculate features 1')

        # numpy array contenente le entropie per ogni Flowset
        rappresentation = np.array([], dtype='f')

        for ip, dictionary in flowset.items():

            ent = 0

            for c in dictionary['packet_count']:
                ent -= c * math.log(abs(c))

            # aggiungo a 'rappresentation' l'entropia appena calcolata
            rappresentation = np.append(arr=rappresentation, values=ent)

        return rappresentation

    # features2: Entropy of time gap foe each flow set
    @staticmethod
    def get_entropy_of_time_gap_foreach_flowset(flowset):

        print('Calculate features 2')

        # numpy array contenente le entropie per ogni Flowset
        rappresentation = np.array([], dtype='f')

        for ip, dictionary in flowset.items():

            # la lista t conterrà i Gap, ossia la differenza (in secondi) tra il tempo di arrivo dell'ultimo pacchetto
            # di un flusso ed il tempo di arrivo del flusso successivo.
            gap = list()

            for (_, f), (s, _) in zip(dictionary['timestamp'][:-1], dictionary['timestamp'][1:]):
                difference = f - s

                gap.append(difference)

            # dictionary['time_gap'] = np.absolute(gap)
            dictionary['time_gap'] = gap

            # inizialmente l'entropia è 0
            ent = 0

            for t in dictionary['time_gap']:

                # TODO: chiedere se va bene

                if t == 0:

                    ent = 0

                else:

                    ent -= t * math.log(abs(t))

            # aggiungo a 'rappresentation' l'entropia appena calcolata
            rappresentation = np.append(arr=rappresentation, values=ent)

        return rappresentation

    @staticmethod
    def get_entropy(rappresentation):

        # nel caso in cui len(rappresentation) è 1, quindi log(1)=0, si andrebbe a dividere per 0,
        # quindi risulterebbe -inf, in questo caso, faccio ritornare 0 (come nel caso in cui vi sono 0 Flowset)
        if len(rappresentation) == 0 or len(rappresentation) == 1:
            return 0

        ent = 0

        for e in rappresentation:

            if e != 0:
                ent -= e * math.log(abs(e))

        # se l'entropia è 0, allora ritorno 0 senza ulteriori calcoli
        if ent == 0:

            return 0

        # altrimenti la computo
        else:

            return np.divide(ent, np.log(len(rappresentation)))

    """
    IMPORTANTE QUA POSSO FARE UN GET TUTTE LE INFO PER IL CALCOLO DI TUTTE LE FEATURES, I RISULTATI ME LI POSSO FAR 
    TORNARE QUI E SCRIVERE SUL PICKLE, PROVARE
    """

    @staticmethod
    def get_flowset(path):
        # TODO: finire
        """
        Contiene la lista degli ip che compongono il Flow Set.

        Nello specifico:
            - vengono contati quanti sono i Flow che vanno verso uno stesso ip di destinazione (Flow count);
            - gli ip di destinazione con un Flow count < 3, non vengono incluse nel Flow set.

        un dict contenente tutte le informazioni per la costruzione di un flow set, nello specifico:
        l'inidirizzo ip di destinazione del flow set;
        l'elenco delle 5-tuple per ogni flow set
        :return: una dict di ip di destinazione con un Flow count >= 3 e della seguente forma:
                {'117.204.73.246':
                    {'count': 3,
                    'files': ['1_17:17:05_17:32:05-0023.pcap',
                              '1_17:17:05_17:32:05-0026.pcap',
                              '1_17:17:05_17:32:05-0282.pcap']},
                '184.173.217.40':
                    {'count': 57,
                    'files': [...]}
                }
        """

        flow_count = {}  # conta quanti flussi ci sono in un Flow Set

        for filecut in sorted(os.listdir(path)):

            file = path + filecut

            first = True

            with open(file, 'rb') as f:

                pcap = dpkt.pcap.Reader(f)

                # For each packet in the pcap process the contents
                for timestamp, buf in pcap:

                    eth = dpkt.ethernet.Ethernet(buf)

                    ip = eth.data

                    # Per ogni file .pcap, considero il primo pacchetto di ogni flusso (perchè il primo pacchetto di
                    # ogni flusso và da un client c verso un server s) ed incrementerè di uno il contatore per quell'ip
                    # di destinazione.
                    if first:

                        # Se la chiave esiste già nel flow_count, allora incremento il contatore per quell'ip dst di 1,
                        # altrimenti creo quella chiave ed attribuisco 1 al numero di Flow.
                        if Utility_PCAP.inet_to_str(ip.dst) in list(flow_count.keys()):

                            flow_count[Utility_PCAP.inet_to_str(ip.dst)]['files'].append(filecut)

                            flow_count[Utility_PCAP.inet_to_str(ip.dst)]['count'] += 1

                        else:

                            flow_count[Utility_PCAP.inet_to_str(ip.dst)] = {}

                            flow_count[Utility_PCAP.inet_to_str(ip.dst)]['files'] = list()

                            flow_count[Utility_PCAP.inet_to_str(ip.dst)]['files'].append(filecut)

                            flow_count[Utility_PCAP.inet_to_str(ip.dst)]['count'] = 1

                        first = False

                    # qui potrei trovare altre caratteristiche
                    
                    # ip.data

                f.close()

        # dopo aver creato il flow_count, list_ip conterrà gli indirizzi ip di destinazione con un Flow count >= di 3.
        for key in list(flow_count.keys()):

            # dal flow_count rimuovo i flussi con un Flow count < di 3.
            if flow_count[key]['count'] < 3:
                del flow_count[key]

        return flow_count

    @staticmethod
    def get_tcp_features(path_flows, path_tcp_flows):

        tcp_features = {}  # conta quanti flussi ci sono in un Flow Set

        ip_packet_count = 0

        for filecut in sorted(os.listdir(path_flows)):

            file = path_flows + filecut

            first = True

            with open(file, 'rb') as f:

                pcap = dpkt.pcap.Reader(f)

                # For each packet in the pcap process the contents
                for timestamp, buf in pcap:

                    try:

                        # Unpack the Ethernet frame (mac src/dst, ethertype)
                        eth = dpkt.ethernet.Ethernet(buf)

                        # Make sure the Ethernet data contains an IP packet
                        if not isinstance(eth.data, dpkt.ip.IP):
                            continue

                        ip_packet_count += 1

                    except Exception as e:
                        print(str(e))

                f.close()

        print("path_tcp_flows: %s" % path_tcp_flows)

        tcp_packet_count = 0

        syn_count = 0

        fin_count = 0

        psh_count = 0

        number_incoming_packet = 0
        number_outgoing_packet = 0

        one_way_packet_count = 0

        list_one_way_packet = list()
        packet_count = 0

        for filecut in sorted(os.listdir(path_tcp_flows)):

            file = path_tcp_flows + filecut

            first = True

            ip_client = 0
            ip_server = 0

            ip_client_count = 0
            ip_server_count = 0

            one_way = True

            with open(file, 'rb') as f:

                pcap = dpkt.pcap.Reader(f)

                # For each packet in the pcap process the contents
                for timestamp, buf in pcap:

                    eth = dpkt.ethernet.Ethernet(buf)

                    ip = eth.data

                    tcp = ip.data

                    tcp_packet_count += 1

                    packet_count += 1

                    if first:
                        ip_client = print_packets.inet_to_str(ip.src)
                        ip_server = print_packets.inet_to_str(ip.dst)

                        number_incoming_packet += 1

                        first = False

                    else:
                        if print_packets.inet_to_str(ip.src) == ip_client:
                            number_incoming_packet += 1
                        elif print_packets.inet_to_str(ip.src) == ip_server:
                            number_outgoing_packet += 1

                    if print_packets.inet_to_str(ip.dst) == ip_client:
                        one_way = False

                    # se il flag SYN è settato
                    if (tcp.flags & dpkt.tcp.TH_SYN) != 0:
                        syn_count += 1

                    # se il flag FIN è settato
                    if (tcp.flags & dpkt.tcp.TH_FIN) != 0:
                        fin_count += 1

                    # se il flag PSH è settato
                    if (tcp.flags & dpkt.tcp.TH_PUSH) != 0:
                        psh_count += 1

                f.close()

            if one_way:
                list_one_way_packet.append(packet_count)

            packet_count = 0

        one_way_packet_count = sum(list_one_way_packet)

        # todo: ricontrollare eccezioni

        try:

            print("Ratio of one-way TCP connections (per time window): %f" % (one_way_packet_count / tcp_packet_count))

            ratio_of_one_way_tcp_connection = one_way_packet_count / tcp_packet_count

        except ZeroDivisionError:

            ratio_of_one_way_tcp_connection = 0

        try:

            print("Ratio of incoming to outgoing TCP packets (per time window): %f" %
                  (number_incoming_packet / number_outgoing_packet))

            ratio_of_incoming_to_outgoing = number_incoming_packet / number_outgoing_packet

        except ZeroDivisionError:

            ratio_of_incoming_to_outgoing = 0

        try:

            print(
                "Ratio of TCP packets to overall packets (per time window): %f" % (tcp_packet_count / ip_packet_count))

            ratio_of_tcp_packets_to_overall_packets = tcp_packet_count / ip_packet_count

        except ZeroDivisionError:

            ratio_of_tcp_packets_to_overall_packets = 0

        tcp_features.__setitem__('features_3', ratio_of_one_way_tcp_connection)
        tcp_features.__setitem__('features_4', ratio_of_incoming_to_outgoing)
        tcp_features.__setitem__('features_5', ratio_of_tcp_packets_to_overall_packets)
        tcp_features.__setitem__('features_6', syn_count)
        tcp_features.__setitem__('features_7', fin_count)
        tcp_features.__setitem__('features_8', psh_count)

        return tcp_features

    @staticmethod
    def flowset_extract_traffic_characterization(path, flow_counter):

        # fs è un dizionario che conterrà informazioni riguardanti tutti i flowset
        fs = OrderedDict()

        for key, dictionary in flow_counter.items():

            dictionary['packet_count'] = list()

            dictionary['timestamp'] = list()

            for filecut in dictionary['files']:

                file = path + filecut

                with open(file, 'rb') as f:

                    list_timestamp = list()

                    c = 0

                    pcap = dpkt.pcap.Reader(f)

                    for timestamp, buf in pcap:

                        c += 1

                        list_timestamp.append(timestamp)

                    dictionary['packet_count'].append(c)

                    min_timestamp = min(list_timestamp)
                    max_timestamp = max(list_timestamp)

                    timestamp_tuple = (min_timestamp, max_timestamp)

                    dictionary['timestamp'].append(timestamp_tuple)

                fs[key] = dictionary

        rappresentation_features1 = FeaturesExtractor.get_entropy_of_packet_counts_foreach_flowset(flowset=fs)

        features1 = FeaturesExtractor.get_entropy(rappresentation=rappresentation_features1)

        rappresentation_features2 = FeaturesExtractor.get_entropy_of_time_gap_foreach_flowset(flowset=fs)

        features2 = FeaturesExtractor.get_entropy(rappresentation=rappresentation_features2)

        return features1, features2, rappresentation_features1, rappresentation_features2
