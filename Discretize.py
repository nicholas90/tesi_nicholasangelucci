import csv

import numpy as np
import pandas as pd

import Graphics
import Utility.UtilityNormalize as UtilityNormalize

"""carica il file csv in un numpy array"""

"""normalizzo"""

"""riaggiungo la colonna con le label"""


def get_features(file_csv):
    import numpy as np

    all_features = []

    # per ogni colonna (tranne l'ultima)
    for i in range(0, 8):

        features = []

        # open csv file
        with open(file_csv) as file:

            read_csv = csv.reader(file, delimiter=',')

            for row in read_csv:
                v = row[i]

                features.append(v)

        features = np.asarray(features)

        all_features.append(features)

    print("---------------")

    all_features = np.vstack(all_features)

    return all_features


def get_dataframe(file_csv):
    path_dataset_csv = UtilityNormalize.get_directory_dataset() + file_csv

    temporary_csv = pd.read_csv(path_dataset_csv, header=None)

    x = temporary_csv.values  # returns a numpy array

    df = pd.DataFrame(x)

    return df


def write_csv(dataframe):
    prova = UtilityNormalize.get_directory_dataset() + "dataset.csv"

    dataframe.to_csv(path_or_buf=prova, header=False, index=False)

    return True


def get_label(file_csv):
    label = 9

    with open(file_csv) as file:
        read_csv = csv.reader(file, delimiter=',')

        for row in read_csv:
            label = row[8]
            break

    return label


def get_normalized_labelled_dataset(normalized_df, labelled_df):
    result_df = pd.concat([normalized_df, labelled_df], axis=1, ignore_index=True)

    return result_df


def split_dataframe(df):
    """
    Splitta un dataframe in due dataframe.

    Il dataframe viene splittato in due dataframe a seconda del valore dell'ultima colonna.

    :param df:
    :return:
    """

    df1 = df[df[8] <= 0.5]
    df2 = df[df[8] >= 0.5]

    return df1, df2


def discretize(data, bins):
    discrete, bins = np.histogram(data, bins, density=False)

    return discrete, bins


def get_discretized_value(arr, bins):
    inds = np.digitize(arr, bins, right=True)

    for c in range(len(inds)):

        # nel caso in cui il valore appartenga al limite inferiore,
        # lo aggrego alla prima categoria (categoria 1)
        if inds[c] == 0:
            inds[c] = 1

    return inds.ravel()

    # for c in range(len(inds)):
    #
    #     # nel caso in cui il valore appartenga al limite inferiore, lo aggrego alla prima categoria
    #     if inds[c][0] == 0:
    #         inds[c] = 1
    #
    # return inds.ravel()


def remove_label_column_dataframe(df):
    del df[8]

    return df


def write_to_csv(csv_name, array):
    # np.savetxt(csv_name, array, delimiter=",")

    with open(csv_name, 'ab+') as file:
        np.savetxt(file, array, delimiter=",")

    return True


def dataset_bins(dataset, number_of_bins):
    all_raw = list()

    for index_column in range(0, 9):

        df = pd.DataFrame(dataset, columns=[index_column])

        if index_column < 8:

            # flat_list = [item for sublist in df.values for item in sublist]

            hist, bins = discretize(df.values, number_of_bins)

            discretized_value = get_discretized_value(arr=df[index_column], bins=bins)

            print(hist)

            # Disegna l'istogramma per ogni features ed il rispettivo numero di bins
            Graphics.get_histogram(feature_number=index_column, bins_number=len(bins), value=hist)

        elif index_column == 8:

            # print(df[index_column])

            discretized_value = df[index_column]
            discretized_value = np.asarray(discretized_value, dtype=int)

        all_raw.append(discretized_value)

    dat_bins = np.asarray(all_raw)

    return np.column_stack(dat_bins)

# def add_label_to_normalize_dataset(normalized_dataset, label):
#
#     minmax_normalized_labelled = np.insert(normalized_dataset, 8, label, axis=1)
#
#     return minmax_normalized_labelled


def run_discretize_7_bins(file_csv):
    # dataset, in formato DataFrame
    dataset = get_dataframe(file_csv=file_csv)

    # discretizzo l'input (quindi le colonne da 0 a 7) preservando l'output
    seven_bins_dataset = dataset_bins(dataset=dataset, number_of_bins=7)

    save = write_to_csv(csv_name="dataset/dataset_7bins.csv", array=seven_bins_dataset)

    if save:
        seven_bins_dataset_df = get_dataframe(file_csv="dataset_7bins.csv")

        # splitto in due dataframe a seconda della label
        normal_label_dataset, botnet_label_dataset = split_dataframe(df=seven_bins_dataset_df)

        seven_bins_normal_dataset = remove_label_column_dataframe(df=normal_label_dataset)

        seven_bins_normal_dataset.hist(column=seven_bins_normal_dataset[0])

        # seven_bins_normal_dataset.hist(column=seven_bins_normal_dataset[1])
        # seven_bins_normal_dataset.hist(column=seven_bins_normal_dataset[2])
        # seven_bins_normal_dataset.hist(column=seven_bins_normal_dataset[3])
        # seven_bins_normal_dataset.hist(column=seven_bins_normal_dataset[4])
        # seven_bins_normal_dataset.hist(column=seven_bins_normal_dataset[5])
        # seven_bins_normal_dataset.hist(column=seven_bins_normal_dataset[6])
        # seven_bins_normal_dataset.hist(column=seven_bins_normal_dataset[7])

        seven_bins_botnet_dataset = remove_label_column_dataframe(df=botnet_label_dataset)

        # seven_bins_botnet_dataset.hist(column=seven_bins_botnet_dataset[0])
        # seven_bins_botnet_dataset.hist(column=seven_bins_botnet_dataset[1])
        # seven_bins_botnet_dataset.hist(column=seven_bins_botnet_dataset[2])
        # seven_bins_botnet_dataset.hist(column=seven_bins_botnet_dataset[3])
        # seven_bins_botnet_dataset.hist(column=seven_bins_botnet_dataset[4])
        # seven_bins_botnet_dataset.hist(column=seven_bins_botnet_dataset[5])
        # seven_bins_botnet_dataset.hist(column=seven_bins_botnet_dataset[6])
        # seven_bins_botnet_dataset.hist(column=seven_bins_botnet_dataset[7])

        write_to_csv(array=seven_bins_normal_dataset, csv_name="dataset/dataset_7bins_normal.csv")

        write_to_csv(array=seven_bins_botnet_dataset, csv_name="dataset/dataset_7bins_botnet.csv")


def run_discretize_20_bins(file_csv):
    # dataset, in formato DataFrame
    dataset = get_dataframe(file_csv=file_csv)

    # discretizzo l'input (quindi le colonne da 0 a 7) preservando l'output
    seven_bins_dataset = dataset_bins(dataset=dataset, number_of_bins=20)

    save = write_to_csv(csv_name="dataset/dataset_20bins.csv", array=seven_bins_dataset)

    if save:
        twenty_bins_dataset_df = get_dataframe(file_csv="dataset_20bins.csv")

        # splitto in due dataframe a seconda della label
        normal_label_dataset, botnet_label_dataset = split_dataframe(df=twenty_bins_dataset_df)

        twenty_bins_normal_dataset = remove_label_column_dataframe(df=normal_label_dataset)

        twenty_bins_normal_dataset.hist(column=twenty_bins_normal_dataset[0])
        twenty_bins_normal_dataset.hist(column=twenty_bins_normal_dataset[1])
        twenty_bins_normal_dataset.hist(column=twenty_bins_normal_dataset[2])
        twenty_bins_normal_dataset.hist(column=twenty_bins_normal_dataset[3])
        twenty_bins_normal_dataset.hist(column=twenty_bins_normal_dataset[4])
        twenty_bins_normal_dataset.hist(column=twenty_bins_normal_dataset[5])
        twenty_bins_normal_dataset.hist(column=twenty_bins_normal_dataset[6])
        twenty_bins_normal_dataset.hist(column=twenty_bins_normal_dataset[7])

        twenty_bins_botnet_dataset = remove_label_column_dataframe(df=botnet_label_dataset)

        twenty_bins_botnet_dataset.hist(column=twenty_bins_botnet_dataset[0])
        twenty_bins_botnet_dataset.hist(column=twenty_bins_botnet_dataset[1])
        twenty_bins_botnet_dataset.hist(column=twenty_bins_botnet_dataset[2])
        twenty_bins_botnet_dataset.hist(column=twenty_bins_botnet_dataset[3])
        twenty_bins_botnet_dataset.hist(column=twenty_bins_botnet_dataset[4])
        twenty_bins_botnet_dataset.hist(column=twenty_bins_botnet_dataset[5])
        twenty_bins_botnet_dataset.hist(column=twenty_bins_botnet_dataset[6])
        twenty_bins_botnet_dataset.hist(column=twenty_bins_botnet_dataset[7])

        write_to_csv(array=twenty_bins_normal_dataset, csv_name="dataset/dataset_20bins_normal.csv")

        write_to_csv(array=twenty_bins_botnet_dataset, csv_name="dataset/dataset_20bins_botnet.csv")
