'''
dpkt issue 254
'''
import dpkt
from dpkt.ip import IP
from dpkt.ethernet import Ethernet
from dpkt.arp import ARP
from pprint import pprint
import socket
import pickle

import Utility_PCAP

file = '/home/nicholas/PycharmProjects/tesi_nicholasangelucci/time_windows/900/botnet-capture-20110815-fast-flux/' \
       '1_16:52:43_17:07:43.pcap'

f = open(file, 'rb')
pcap = dpkt.pcap.Reader(f)

def ip_to_str(address):
    """
    transform a int ip address to a human readable ip address (ipv4)
    """
    return socket.inet_ntoa(address)


class Flow(object):
    '''
    Code from Honeysnap
    https://github.com/honeynet
    '''
    def __init__(self):
        self.name = None
        self.src = None
        self.dst = None
        self.sport = None
        self.dport = None
        self.numpacket = 0

    def __eq__(self, other):
        return self.sport == other.sport and self.dport == other.dport and self.src == other.src and \
               self.dst == other.dst

    def __ne__(self, other):
        return self.sport != other.sport or self.dport != other.dport or self.src != other.src or self.dst != other.dst

    def __repr__(self):
        return "%s:%s-%s:%s" % (self.src, self.sport, self.dst, self.dport)

    def isSrcSport(self, src, sport):
        if self.src == src and self.sport == sport:
            return True
        else:
            return False

    def print_flow(self):

        print(self.src, self.sport, self.dst, self.dport, self.numpacket)
