import os
import shutil
import sys

import dpkt

import Run_Program
import Utility.UtilityPreprocessing as UtilityPreprocessing
import Utility.Utils as Utils

"""Classe Preprocessing, si occupa preprocessare il file .pcap in input.

In altre parole, preparare il file in input alla conseguente estrazione delle features

Attributes:
    :param directory_input: directory contenente il file in input.
    :param tw_duration: durata delle time-windows.
"""


class Preprocessing(object):

    def __init__(self, directory_input, tw_duration):

        self.directory_input = directory_input
        self.tw_duration = tw_duration

    def split_in_flows(self, folder_input):
        """
        Splitta in flussi.

        Se nella cartella di destinazione sono presenti dei file, li rimuove.

        :param folder_input: nome della cartella 'NOME_FILE_INPUT'
        :return: directory contenente tutti i flussi 'preprocessed/[NOME_FILE_INPUT]/all_flows/'
        """

        # contiene il path della directory '/all_flows'
        directory_all_flows = UtilityPreprocessing.get_directory_all_flows(folder_file_input=folder_input)

        # se 'directory' non esiste, la crea
        Utils.check_directory(directory=directory_all_flows)

        Utils.remove_all_files_on_directory(directory_all_flows)

        Run_Program.run_pcapplitter_split5tupla(path_file_pcap=self.directory_input, path_5tuple=directory_all_flows)

        return directory_all_flows

    @staticmethod
    def get_end_time_of_time_windows(directory_file_input, time_windows_duration):
        """
        Crea un array contenente tutti i timestamp di fine, per ogni time-windows.

        :param directory_file_input: directory contenente il file .pcap in input.
        :param time_windows_duration: durata delle time-windows (espressa in secondi)
        :return: list contenente i timestamp di fine per ogni time-windows.
        """

        # contiene una tuple contenente ii time-stamp di inizio e fine time-windows
        timestamp_input_file = Preprocessing.get_timestamps(file_pcap=directory_file_input)

        timestamp_start_input_file = timestamp_input_file[0]
        timestamp_end_input_file = timestamp_input_file[1]

        # durata del file .pcap in input (espressa in secondi)
        input_file_duration = timestamp_end_input_file - timestamp_start_input_file

        print("Durata del file .pcap in input, espressa in secondi: %d" % input_file_duration)

        number_of_time_windows = input_file_duration / time_windows_duration

        print("Numero di time-windows: %d" % (int(number_of_time_windows) - 1))

        # array contenente i timestamp di fine di ogni time-windows
        timestamp_end_timewindows = []

        current_timestamp = timestamp_start_input_file

        # creo una lista conetenente tutti i time-stamp di fine time-windows
        for t in range(int(number_of_time_windows)):
            current_timestamp = current_timestamp + time_windows_duration

            timestamp_end_timewindows.append(current_timestamp)

        return timestamp_end_timewindows

    @staticmethod
    def get_timestamps(file_pcap):
        """
        Calcola i time-stamp di inizio e fine del file .pcap (quindi della 5-tupla)
        passatogli in input.

        :param file_pcap: 5-tupla in input.
        :return: una tupla contenente i time-stamp di inizio e fine.
        """

        with open(file_pcap, 'rb') as f:
            try:

                pcap = dpkt.pcap.Reader(f)

                min_timestamp = sys.maxsize
                max_timestamp = 0

                for timestamp, buf in pcap:
                    min_timestamp = min(min_timestamp, timestamp)
                    max_timestamp = max(max_timestamp, timestamp)

            except Exception as e:
                sys.exit("[!] WARNING: Errore: %s; molto probabilmente il file che hai passato in input è danneggiato, "
                         "oppure è stato salvato in un formato non valido e la libreria dpkt non è in grado di leggerlo"
                         "; riprova a scaricarlo [TODO suggerire conversione]." % e)

        return min_timestamp, max_timestamp

    def aggregate_in_time_windows(self, directory_all_flows, directory_time_windows):
        """
        Aggrega i flows in time-windows.

        :param directory_all_flows: directory contenente tutti i flussi.
        :param directory_time_windows: directory '/time-windows'.
        :return: ritorna la directory contenente tutto ciò che riguarda una determinata time-windows:
        'preprocessed/[NOME_FILE_INPUT]/time-windows/[DURATA_TIME_WINDOWS]/'
        """

        # contiene il path della folder relativa ad una determinata durata di time-windows
        directory_timewindows_duration = UtilityPreprocessing.get_directory_timewindows_duration(
            directory_time_windows=directory_time_windows, time_windows_duration=self.tw_duration)

        Utils.check_directory(directory=directory_timewindows_duration)

        # rimuovo tutto il contenuto dal path "directory_timewindows_duration"
        Utils.remove_all_folder_on_directory(directory_timewindows_duration)

        timestamp_end_timewindows = Preprocessing.get_end_time_of_time_windows(
            directory_file_input=self.directory_input, time_windows_duration=self.tw_duration)

        """ Implementazione algoritmo di aggregazione in time-winwows """

        for filecut in os.listdir(directory_all_flows):

            file = directory_all_flows + filecut

            flow_start_timestamp = Preprocessing.get_timestamps(file_pcap=file)[0]

            if flow_start_timestamp < min(timestamp_end_timewindows):

                directory_preprocessed_timewindows = UtilityPreprocessing.get_directory_preprocessed_timewindows(
                    directory_time_windows_duration=directory_timewindows_duration, name_timewindows=0)

                Utils.check_directory(directory=directory_preprocessed_timewindows)

                path_timewindows_flows = UtilityPreprocessing.get_directory_preprocessed_timewindows_flows(
                    directory_preprocessed_timewindows=directory_preprocessed_timewindows)

                Utils.check_directory(directory=path_timewindows_flows)

                shutil.copy2(file, path_timewindows_flows)

            elif flow_start_timestamp > max(timestamp_end_timewindows):

                pass

            else:

                for index, timestamp in enumerate(timestamp_end_timewindows[1:-1]):

                    name_timewindows = str(index + 1)

                    if timestamp_end_timewindows[index] < flow_start_timestamp <= timestamp_end_timewindows[index + 1]:

                        directory_preprocessed_timewindows = \
                            UtilityPreprocessing.get_directory_preprocessed_timewindows(
                                directory_time_windows_duration=directory_timewindows_duration,
                                name_timewindows=name_timewindows)

                        Utils.check_directory(directory=directory_preprocessed_timewindows)

                        path_timewindows_flows = UtilityPreprocessing.get_directory_preprocessed_timewindows_flows(
                            directory_preprocessed_timewindows=directory_preprocessed_timewindows)

                        Utils.check_directory(path_timewindows_flows)

                        shutil.copy2(file, path_timewindows_flows)

        return directory_timewindows_duration

    def filter_tcp_flows(self, directory_timewindows_duration):

        """
        Verifica se il file .pcap è flusso TCP.

        Se lo è, rimane in nella directory dove si trova, ossia 'path_flows';
        altrimenti viene spostato nella directory 'path_discarded_flows'.

        :param directory_timewindows_duration: path contenente tutto ciò che riguarda una determinata time-windows.
        :return: true
        """

        timewindows = UtilityPreprocessing.get_list_of_timewindows(
            directory_preprocessed_timewindows=directory_timewindows_duration)

        # per ogni time-windows
        for name in timewindows:
            directory_preprocessed_timewindows = UtilityPreprocessing.get_directory_preprocessed_timewindows(
                directory_time_windows_duration=directory_timewindows_duration, name_timewindows=name)

            Utils.check_directory(directory=directory_preprocessed_timewindows)

            # TODO: documentare
            directory_flows = UtilityPreprocessing.get_directory_preprocessed_timewindows_flows(
                directory_preprocessed_timewindows=directory_preprocessed_timewindows)

            Utils.check_directory(directory=directory_flows)

            # get_directory_preprocessed_timewindows_flows [TODO]
            directory_tcp_flows = UtilityPreprocessing.get_directory_preprocessed_timewindows_tcp_flows(
                directory_preprocessed_timewindows=directory_preprocessed_timewindows)

            Utils.check_directory(directory=directory_tcp_flows)

            # TODO: documentare
            directory_discarded_tcp_flows = \
                UtilityPreprocessing.get_directory_preprocessed_timewindows_discarded_tcp_flows(
                    directory_preprocessed_timewindows=directory_preprocessed_timewindows)

            Utils.check_directory(directory=directory_discarded_tcp_flows)

            self.tcp_filter(directory_flows=directory_flows, directory_tcp_flows=directory_tcp_flows,
                            directory_discarded_tcp_flows=directory_discarded_tcp_flows)

        return True

    def tcp_filter(self, directory_flows, directory_tcp_flows, directory_discarded_tcp_flows):

        # contiene la lista dei file contenuti nella cartella '/flows/'
        files_folder_flows = [f for f in os.listdir(directory_flows) if f.endswith(".pcap")]

        # per ogni file in files_folder_flows
        for f in files_folder_flows:

            # contiene il path del flusso
            file = directory_flows + f

            # se è True, il flusso è TCP
            is_tcp_flow = False

            with open(file, 'rb') as f:

                pcap = dpkt.pcap.Reader(f)

                # For each packet in the pcap process the contents
                for timestamp, buf in pcap:

                    try:

                        # Unpack the Ethernet frame (mac src/dst, ethertype)
                        eth = dpkt.ethernet.Ethernet(buf)

                        # Make sure the Ethernet data contains an IP packet
                        if not isinstance(eth.data, dpkt.ip.IP):
                            continue

                        # Now grab the data within the Ethernet frame (the IP packet)
                        ip = eth.data

                        # Check for TCP in the transport layer
                        if not isinstance(ip.data, dpkt.tcp.TCP):
                            continue

                        # nel caso in cui in un flusso sono presenti pacchetti tcp, is_tcp_flow viene settato a True
                        is_tcp_flow = True

                        if is_tcp_flow:
                            continue

                    except Exception as e:
                        print(str(e))

            f.close()

            # se nel flusso, sono presenti pacchetti TCP, allora viene spostato nella cartella contenente flussi TCP
            # altrimenti, viene spostato nella cartella dei flussi scartati.
            if is_tcp_flow:
                shutil.copy2(file, directory_tcp_flows)
            else:
                shutil.copy2(file, directory_discarded_tcp_flows)

    def run_preprocessing(self):

        # contiene il nome della cartella dove verranno fatte tutte le operazioni sul file in input
        folder_input = Utils.get_name_file_input(directory_file_input=self.directory_input)

        """ Split in flow """

        directory_all_flows = self.split_in_flows(folder_input=folder_input)

        """ Aggregate in time-windows """

        # contiene il path cotenente le time-windows
        directory_timewindows = UtilityPreprocessing.get_directory_time_windows(folder_file_input=folder_input)

        Utils.check_directory(directory=directory_timewindows)

        directory_timewindows_duration = self.aggregate_in_time_windows(
            directory_all_flows=directory_all_flows, directory_time_windows=directory_timewindows)

        """ Filter """

        filter_tcp_flows = self.filter_tcp_flows(directory_timewindows_duration=directory_timewindows_duration)

        if filter_tcp_flows:
            print("""End preprocessing.""")
