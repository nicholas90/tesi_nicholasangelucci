import os
import sys
from sys import version_info

import Utility.UtilityMain as UtilityMain
import UtilityDB
from Orchestrator import Orchestrator
from Parser import Parser


def main():

    print('Start System')  # OR Start Intrusion_Detection-learn for Botnet

    py3 = version_info[0] > 2  # crea un valore booleano per testare che l'interprete Python attivo sia > 2

    if py3:

        os.chmod(os.getcwd(), 0b111111111)

        UtilityMain.check_directories()

        # UtilityDB.unlock_db(db_filename="database.db")

        # TODO: sono da unire

        if UtilityDB.create_table() is None:
            sys.exit(1)

        if UtilityDB.create_table_dataset_composition() is None:
            sys.exit(1)

        args = sys.argv

        parser = Parser(args)

        tuples_args = parser.execute()

        orchestrator = Orchestrator(tuples_args)

        orchestrator.run_orchestrator()

    else:

        sys.exit("[!] ERRORE: Stai usando un interprete < di python 3; il codice non è compatibile per quell'interprete"
                 "; riprova ad eseguire il codice con un'interprete per Python 3 o successivi.")


if __name__ == "__main__":

    main()
