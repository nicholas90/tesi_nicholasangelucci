import os

# cmd = '''tshark -r %(nomeFile)s -Y '((frame.time >= "2011-08-15 16:52:43") &&
# (frame.time <= "2011-08-15 17:07:43"))' -w 1.pcap''' % {'nomeFile': nomefile}


# lancia tshark, con lo scopo di dividere il pcap in finestre temporali
def run_tshark(nomeFileInput, timestamp_inizio_tw, timestamp_fine_tw, nomeFileOutput):

    print('''File generato: %s''' % os.path.basename(nomeFileOutput))

    cmd = '''tshark -r %(nomeFileInput)s -Y '((frame.time >= "%(timestampInizio_tw)s") && 
    (frame.time <= "%(timestampFine_tw)s"))' -F pcap -w %(nomeFileOutput)s''' \
          % {'nomeFileInput': nomeFileInput, 'timestampInizio_tw': timestamp_inizio_tw,
             'timestampFine_tw': timestamp_fine_tw, 'nomeFileOutput': nomeFileOutput}

    os.system(cmd)


# TODO: importante vedere eccezioni e valore di ritorno
def run_splitcap_splitInBidirectionalFlows(path_filePCAP, path_bidirectional_flows):

    #print("path_filePCAP: ", path_filePCAP)
    '''importante'''
    #filePCAP = path_filePCAP.split("/")[-1]
    #print("filePCAP: %s" % filePCAP)

    splitcap_path = 'SplitCap_2-1/SplitCap.exe'  # TODO: (chiedere) fare un metodo che lo ritorna

    print("[RUN COMMAND] mono %s -r %s -o %s -s hostpair" % (splitcap_path, path_filePCAP, path_bidirectional_flows))

    os.system("mono %s -r %s -o %s -s hostpair" % (splitcap_path, path_filePCAP, path_bidirectional_flows))

    return path_bidirectional_flows


def run_splitcap_splitIn5tupla(path_filePCAP, path_5tupla):
    # print("path_filePCAP: ", path_filePCAP)
    '''importante'''
    # filePCAP = path_filePCAP.split("/")[-1]
    # print("filePCAP: %s" % filePCAP)

    splitcap_path = 'SplitCap_2-1/SplitCap.exe'  # TODO: (chiedere) fare un metodo che lo ritorna

    print(
        "[RUN COMMAND] mono %s -r %s -o %s -s flow" % (splitcap_path, path_filePCAP, path_5tupla))

    """
        TODO: posso provare con questo comando, e il risultato di questo cmando è uguale a quello ottenuto con il modulo
        filtro.
        SplitCap -r dumpfile.pcap -s flow -y L7 
    """

    os.system("mono %s -r %s -o %s -s flow" % (splitcap_path, path_filePCAP, path_5tupla))

    return path_5tupla


def run_pcapplitter_split5tupla(path_file_pcap, path_5tuple):

    """
        Lancia il programma PcapSplitter per splittare un file .pcap in 5-tuple.
        Prima di lanciarlo, va installato con il comando sudo ./install.sh

        :param path_file_pcap:
        :param path_5tuple:
        :return:
    """
    # PcapSplitter [-h] [-v] [-i filter] -f pcap_file -o output_dir -m split_method

    pcapsplitter_path = 'pcapplusplus/examples/PcapSplitter'  # TODO: (chiedere) fare un metodo che lo ritorna

    print(
        "[RUN COMMAND] %s -f %s -o %s -m connection" % (pcapsplitter_path, path_file_pcap, path_5tuple))

    os.system("%s -f %s -o %s -m connection" % (pcapsplitter_path, path_file_pcap, path_5tuple))

    return path_5tuple


def run_splitcap_splitInSession(path_filePCAP, path):

    splitcap_path = 'SplitCap_2-1/SplitCap.exe'  # TODO: (chiedere) fare un metodo che lo ritorna

    print("[RUN COMMAND] %s -r %s -o %s -s session" % (splitcap_path, path_filePCAP, path))

    # SplitCap.exe -r huge.pcap -s session

    os.system("mono %s -r %s -o %s -s session" % (splitcap_path, path_filePCAP, path))

    return path
