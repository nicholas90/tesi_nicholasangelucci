import dpkt

from scapy.all import *

from pprint import pprint

""""{IP:%IP.src%:%TCP.sport% -> %IP.dst%:%TCP.dport%\n}"""

list = []


def action(pkts):
    """return "%s:%s-%s:%s" % (IP.src, self.sport, self.dst, self.dport)"""

    for packet in pkts:

        # print(packet.show())

        if packet.haslayer('IP'):
            # print(packet[IP].src, packet[IP].dst, packet[IP].proto)
            if packet.haslayer('TCP'):

                key = "%s:%s-%s:%s" % (packet['IP'].src, packet['IP'].sport, packet['IP'].dst, packet['IP'].dport)

                # print(key.split("-")[0], ' ', key.split("-")[1])

                if key:

                    revertkey = "%s-%s" % (key.split("-")[1], key.split("-")[0])

                    # if not flows.has_key(key):
                    if key not in list:

                        list.insert(len(list), key)


path = '/home/nicholas/PycharmProjects/tesi_nicholasangelucci/pcaps/botnet-capture-20110815-fast-flux.pcap'

pkts = sniff(offline=path, prn=action)

pprint(list)
print('length', len(list))
