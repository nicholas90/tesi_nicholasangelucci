## Readme - Tesi Nicholas Angelucci

---

## Dipendenze

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Esecuzione dei singoli moduli.

Lanciare il file main.py seguito da uno dei parametri che si desidera utilizzare.

1. **-p**: serve per chiamare il modulo di pre-processing (sulla coppia che gli viene passata come parametro).
        Deve essere seguito obbligatoriamente da due parametri, il primo è il nome del file .pcap che si desidera 
        preprocessare (deve essere situato all'interno della cartella pcaps/) mentre il secondo è la durata delle 
        time-windows nelle quali si vuole preprocessare il file (espressa in secondi).
        Al termine della procedura, il file e durata delle time-windows nel quale è stato preprocessato, verranno 
        memorizzate nel DB. Se un utente prova a preprocessare una coppia (nome_file, tw) già preprocessata in 
        precedenza, viene avvisato, in modo valutare se preprocessare di nuovo, quella determinata coppia. Esempio:
        
        -p capture1-first-normal-traffic.pcap 900
        
2. **-f**: serve per chiamare il modulo di estrazione delle features (sulla coppia che gli viene passata come 
        parametro), una volta estratte, se presente il -l verranno salvate in dataset provvisori insieme alle label.
        I parametri primi due parametri che gli possono essere passati sono gli stessi che devono essere passati al 
        modulo -p, inoltre la coppia sulla quale deve essere chiamato questo modulo deve essere stata preprocessata in 
        precedenza. Inoltre, accetta: il parametro **-graph** seguito dal nome della categoria del file (le categorie 
        previste sono **normal** oppure **botnet**), mediante il quale, dopo l'estrazione delle features verranno 
        creati dei grafici con lo scopo di illustrare graficamente i valori estratti e/o il parametro -l seguito dalla 
        categoria del file e da due nomi di file .csv, il primo conterrà i valori per le features estratte, mentre il 
        secondo conterrà le label.
        Esempio:
        
        -f capture1-first-normal-traffic.pcap 900 -graph normal -l normal dataset_nl.csv dataset_l.csv

3. **-n**: serve per chiamare il modulo che si occupa della normalizzazione e della creazione del dataset da dare in 
        pasto alla rete neurale.
        I parametri che gli possono essere passati sono i due dataset **temporanei** ottenuti applicando il modulo 
        illustrato precedentemente su ogni file .pcap che andrà a comporre il dataset; in ordine deve essere passato 
        prima il file .csv contenente i valori da normalizzare e poi il dataset contenente le label.
        Nello specifico si occupa di normalizzare i valori contenuti nel primo file .csv (per ogni colonna), di 
        attribuirgli le corretta label (contenuta nel secondo file .csv), e di salvare il dataset nella cartella 
        'save/' sotto il nome di **dataset.csv**.
        Esempio:
        
        -n normal dataset_nl.csv dataset_l.csv
        
4. **-d**: [modulo di discretizzazione]

5. **-nn**: modulo di classificazione, implementa la rete neurale, per ora, non è integrato, per eseguirlo, basta 
        avviare eseguire il file **NeuralNet.py**.

---

## Aggiornare il codice alle ultime modifiche effettuate.

Per rimanere sincronizzati con le ultime modifiche effettuate, il repository è disponibile su 
[Tesi Nicholas Angelucci](https://bitbucket.org/nicholas90/tesi_nicholasangelucci/src/master/).