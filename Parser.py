import sys

import Utility.Utils as Utils
import UtilityDB
from Switch import Switch

"""Class Parser, si occupa della creazione di un Parser.

L'oggetto Parser si occupa dell'interfaccia a riga di comando.
Prende i parametri da inseriti dall'utente da terminale (dove ogni parametro rappresenta una funzionalità messa a 
dispozione dal programma) i quali, saranno memorizzati in args ed indicano: qual'è la funzionalità che l'utente vuole 
utilizzare, seguita dai relativi parametri.
La lista args viene trasformata in tuple, nello specifico ogni tupla avrà la seguente forma:
-[nome funzionalità_1] [parametro_1] [parametro_2] [...] -[nome funzionalità_1] [parametro_1] [...] [etc]. 
Mediante appositi metodi, vengono eseguiti una serie di test:
- verifica se la funzione richiesta viene realmente messa a disposizione dal sistema
- controlla se la lunghezza di ogni tupla è giusta
- se i parametri passati sono presenti realmente nella cartella.
Eseguite queste operazioni (quindi una volta acquisiti questi dati, trasformati in tuple e controllata la loro 
veridicità), vengono passati all'oggetto Orchestrator.

Args:
    args: lista contenente le funzionalità (ed i rispettivi parametri) che l'utente può passare al file "main.py", 
    nel momento in cui viene eseguito.
    Le funzionalità (ed i rispettivi parametri), che è possibile passare al Parser, sono:
    -p: (seguito obbligatoriamente da due parametri), serve per chiamare il modulo di preprocessing, 
        splitta il file .pcap che segue il -p (e che a sua volta, si trova nella cartella '/pcaps'), in time-windows di 
        durata minore-uguale (espressa in minuti) al secondo parametro che prende il -p; al termine della procedura, il 
        il file e la tw, verranno memorizzate nel DB.
        Se un utente prova a preprocessare una coppia (nome_file, tw) già preprocessata in precedenza, viene avvisato, 
        in modo valutare se preprocessare di nuovo, quella determinata coppia.
    -f: (seguito obbligatoriamente da due parametri), serve per chiamare il modulo di estrazione delle features, 
        controlla se il file .pcap (primo parametro che segue il -f), e la time-windows (secondo parametro che segue il 
        -f) sono stati già "preprocessati" (quindi se sono già passati per il modulo di preprocessing); se lo sono, 
        verranno estratte le features [
        
        EVENTUALMENTE, SPOSTO QUI I FILTER
        
        ]; altrimenti, l'esecuzione termina con un messaggio di warning.
    -pf: 
    -w: 
    -r: rimuove i dati dal database [NON IMPLEMENTATO]
"""

# TODO: gestire parametri args non validi => penso lo ho fatto
# TODO: implementare l'helper -h


class Parser(object):

    def __init__(self, args):
        self.args = args

    def __str__(self):
        str(self.args)

    def addArgument_parser(self, args):
        """
        Setta il parametro args dell'oggetto Parser, assegnadogli la lista args he gli viene passata in input.

        :param args: lista di argomenti
        :return:
        """
        self.args = args

    def convert_arguments_in_tuples(self):
        """
        Converte la lista di argomenti, memorizzata nel parametro args dell'oggetto Parser, in tuple.

        :return: Una lista di tuple, ogni tupla contiene un valore passato da terminale con i rispettivi parametri.
        Esempio: [('-p', 'file1.pcap', 900)]
        """

        splitted_args = []
        counter = 1

        while counter < len(self.args):
            if self.args[counter][0] == "-":
                next_arg = counter + 1

                # se ci sono argomenti senza "-" li prende
                while next_arg < len(self.args) and self.args[next_arg][0] != "-":
                    next_arg += 1

                # inserisco nella tupla, gli argomenti (cioè il/i carattere/i col -, più il/i suo/suoi parametro/i)
                splitted_args.append(tuple(self.args[counter:next_arg]))
                counter = next_arg

            # se al primo argomento che trova non c'è il "-"
            else:
                splitted_args.append((self.args[counter],))
                counter += 1

        if not splitted_args:
            sys.exit("[!] WARNING: non hai passato nessuno degli argomenti da riga di comando, al file main.py! "
                     "Per favore consulta l'elenco delle funzionalità messe a disposizione dal programma, "
                     "utilizzando il flag -h [TODO: da implementare]")

        return splitted_args

    """V
      
    Raises:
        ValueError: Uno dei parametri inseriti non corrisponde a nessuna funzionalità.
    """
    # TODO: i break servono per usufruire di una singola funzionalità, se li tolgo, posso usufruire di più funzionalità
    def valuate_existence_functionality(self, tuples):

        """
        Valuta la veridicità delle funzionalità richieste dall'utente.

        Valuta se le funzionalità richieste dall'utente da terminale, sono realmente messe a disposizione dal sistema.

        :param tuples:
        :raises: ValueError - 
        :return:
        """

        try:

            b = False

            for t in tuples:

                v = t[0]

                if b:
                    break

                for case in Switch(v):
                    if case('-p'):
                        b = True
                        break
                    if case('-f'):
                        if len(tuples) > 1:
                            if self.valuate_existence_f(tuples[1:]):
                                b = True
                                break
                            else:
                                raise ValueError('[!] WARNING: Uno dei flag che segue il -f '
                                                 'non corrisponde a nessuna funzionalità!')
                        else:
                            b = True
                            break
                    if case('-n'):
                        b = True
                        break
                    if case('-nl'):
                        b = True
                        break
                    if case('-d'):
                        b = True
                        break
                    if case('-nn'):
                        b = True
                        break
                    if case('-r'):
                        b = True
                        break
                    if case():  # default, could also just omit condition or 'if True'
                        raise ValueError('[!] WARNING: Uno dei flag inseriti non corrisponde a nessuna funzionalità!')
                        # No need to break here, it'll stop anyway
        except ValueError as e:
            sys.exit(e)

    def valuate_existence_f(self, tuples):
        """
        -f e -graph
        -f -graph -l -s

        :param tuples:
        :return:
        """

        try:

            for t in tuples:

                v = t[0]

                for case in Switch(v):
                    if case('-graph'):
                        break
                    if case('-l'):
                        break
                    if case('-w'):
                        break
                    if case():  # default, could also just omit condition or 'if True'
                        raise ValueError
                        # No need to break here, it'll stop anyway
        except ValueError:
            return False
        return True

    """Valuta se la lunghezza delle tuple è quella che ci si aspetta.

    Ad esempio: 
    Se inseriamo il parametro -l, ci aspettiamo che sia seguita da due path (ossia quello del file pcap e quello con il 
    file che contiene le label).
    Se non è così, l'esecuzione termina, con il un messaggio di errore.

    Args:
        Tuples: Lista di tuple.
        keys: A sequence of strings representing the key of each table row
            to fetch.
        other_silly_variable: Another optional variable, that has a much
            longer name than the other args, and which does nothing.

    Returns:
        A dict mapping keys to the corresponding table row data
        fetched. Each row is represented as a tuple of strings. For
        example:

        {'Serak': ('Rigel VII', 'Preparer'),
         'Zim': ('Irk', 'Invader'),
         'Lrrr': ('Omicron Persei 8', 'Emperor')}

        If a key from the keys argument is missing from the dictionary,
        then that row was not found in the table.

    Raises:
        ValueError: Occorre quando il numero di parametri passato ad un'argomento è errato
    """

    def valuate_length_of_tuple(self, tuples):

        # TODO: valutare quest'altro tipo di Swith:
        # TODO: https://www.pydanny.com/why-doesnt-python-have-switch-case.html

        try:

            for t in tuples:
                v = t[0]

                for case in Switch(v):

                    if case('-p'):
                        if len(t)-1 != 2:
                            raise ValueError("Il numero di parametri che hai passato a -p è errato, devi passargli due "
                                             "parametri; riprova!")
                        break
                    if case('-f'):  # fatto
                        if len(t) - 1 != 2:
                            raise ValueError("Il numero di parametri che hai passato a -f è errato, devi passargli due "
                                             "parametri; riprova!")
                        break
                    if case('-graph'):  # fatto
                        if len(t) - 1 != 1:
                            raise ValueError("Il numero di parametri che hai passato a -graph è errato, devi passargli "
                                             "un parametro; riprova!")
                        break
                    if case('-l'):  # fatto
                        if len(t) - 1 != 3:
                            raise ValueError("Il numero di parametri che hai passato a -l è errato, devi passargli tre "
                                             "parametri; riprova!")
                        break
                    # if case('-w'):
                    #     if len(t) - 1 != 2:
                    #         raise ValueError("Il numero di parametri che hai passato a -w è errato, devi passargli due "
                    #                          "parametri; riprova!")
                    #     break
                    if case('-n'):
                        print(len(t) - 1)
                        if len(t) - 1 != 2:
                            raise ValueError(
                                "Il numero di parametri che hai passato a -n è errato, devi passargli due parametri; "
                                "riprova!")
                        break
                    if case('-nl'):
                        if len(t) - 1 != 1:
                            raise ValueError(
                                "Il numero di parametri che hai passato a -l è errato, devi passargli  1 - due "
                                             "parametri; riprova!")
                        break
                    if case('-d'):
                        if len(t) - 1 != 1:
                            raise ValueError("Il numero di parametri che hai passato a -d è errato, devi passargli un "
                                             "parametro; riprova!")
                        break
                    if case('-r'):
                        if len(t) - 1 != 0:
                            raise ValueError(
                                "Il numero di parametri che hai passato a -r è errato, non devi passargli "
                                "parametri. Riprova!")
                        break
                    if case():  # default, could also just omit condition or 'if True'
                        print("something else!")
                        # No need to break here, it'll stop anyway
        except ValueError as e:
            sys.exit(e)

    """Valuta il numero di parametri (? il contenuto quindi), di ogni possibile opzione.

    Valuta se il numero di parametri passati ad ogni argomento (da teminale) è quello che ci si aspetta in modo che 
    l'esecuzione possa continuare senza problemi.

    Valuta se il numero di parametri delle tuple è quella che ci si aspetta.


    Ad esempio: 
    Se inseriamo -l, ci aspettiamo che sia seguito da due path (ossia quello del file pcap e quello con il 
    file che contiene le label). Se non è così, l'esecuzione termina.

    Args:
        Tuples: Lista di tuple.
        keys: A sequence of strings representing the key of each table row
            to fetch.
        other_silly_variable: Another optional variable, that has a much
            longer name than the other args, and which does nothing.

    Returns:
        A dict mapping keys to the corresponding table row data
        fetched. Each row is represented as a tuple of strings. For
        example:

        {'Serak': ('Rigel VII', 'Preparer'),
         'Zim': ('Irk', 'Invader'),
         'Lrrr': ('Omicron Persei 8', 'Emperor')}

        If a key from the keys argument is missing from the dictionary,
        then that row was not found in the table.

    Raises:
        IOError: An error occurred accessing the bigtable.Table object.
    """

    def valuate_content_of_tuple(self, tuples):

        b = False

        for t in tuples:
            v = t[0]

            if b:
                break

            for case in Switch(v):

                if case('-nl'):
                    try:
                        open(t[1])
                    except FileNotFoundError:
                        print('Il file', str(t[1]), 'non è presente nella directory di lavoro')
                        exit(0)
                    finally:
                        break

                if case('-p'):

                    # filePCAP contiene il path del file .pcap (passato al -t)
                    file_pcap = Utils.get_directory_pcaps() + t[1]

                    # tw_duration contiene la durata della tw (quindi il secondo argomento passato a -t)
                    tw_duration = int(t[2])

                    # print(file_pcap)

                    # verifico se il file .pcap è realmente presente nel path
                    try:
                        open(file_pcap)
                    except FileNotFoundError:
                        sys.exit('Il file %s non è presente nella directory di lavoro' % (str(t[1])))

                    # TODO: inserire un controllo sulla lunghezza del file .pcap in input, se è < 900 sec non può
                    # TODO: essere analizzato

                    if tw_duration in [10, 900]:
                        break
                    else:
                        sys.exit('[!] WARNING: La durata che hai scelto per le time-windows, cioè %s, non può essere '
                                 'selezionata, scegline una tra 10 e 900 secondi' % t[2])
                    # finally:
                    #     break

                if case('-f'):

                    """
                    Questo è il caso in cui è stato richiesto solamente il modulo -f, (significa che il modulo di 
                    'preprocessing') è già stato utilizato su quel file in input, e per quella time-windows; quindi, 
                    verifico se la tupla (nomeFile, tw) è presente nel DB; se non lo è restituisco un messaggio di 
                    'warning', altrimenti, passo al punto successivo.
                    """

                    value = UtilityDB.execute_select_tuple(nome_file=t[1], tw=int(t[2]))
                    
                    # caso in cui viene sollevata l'eccezione
                    if value is None:
                        sys.exit(1)
                    # caso in cui la tupla (nomeFile, tw) non è presente nel DB
                    elif not value:
                        sys.exit('[!] WARNING: la tupla (nomeFile, tw) non è presente nel DB, quindi il file %s non '
                                 'è stato preprocessato per la time-window %s; usa il flag -p per pre-processare la '
                                 'tupla (nomeFile, tw) e riprova.' % (t[1], t[2]))

                    if len(tuples) > 1:
                        if self.valuate_content_of_f(tuples[1:]):
                            b = True
                            break
                        else:
                            raise ValueError('[!] WARNING: Uno dei flag che segue il -f '
                                             'non corrisponde a nessuna funzionalità!')
                    else:
                        b = True
                        break
                #
                # if case('-graph'):
                #     if tuples[0][1] not in ['normal', 'botnet']:
                #         sys.exit("[!] WARNING: a -graph devi passare come argomento 'normal' o 'botnet'.")

                """
                if case('-pf'):
                    
                    # uguale a case -p ma senza case -f
                    # motivo: se chiami prima il modulo di preprocessing, allora, la tuple (nomeFile, tw) è già 
                    # stata preprocessata, quindi è pesente nel db
                    
                    break
                """
                if case('-r'):

                    # controllo se il database esiste all'nterno del progetto
                    database = UtilityDB.get_database()

                    # verifico se il database è realmente presente all'interno della cartella del progetto
                    try:
                        open(database)
                    except FileNotFoundError:
                        sys.exit('Il file %s non è presente nella directory di lavoro' % database)

                    break

                # if case('-d'):
                #
                #     # controllo se il database esiste all'nterno del progetto
                #     database = UtilityDB.get_database()
                #
                #     # verifico se il database è realmente presente all'interno della cartella del progetto
                #     try:
                #         open(database)
                #     except FileNotFoundError:
                #         sys.exit('Il file %s non è presente nella directory di lavoro' % database)
                #
                #     break

                if case('eleven'):
                    print(11)
                    break

                if case('-n'):
                    break

                if case('-d'):
                    break

                if case():  # default, could also just omit condition or 'if True'
                    print("something else!")
                    # No need to break here, it'll stop anyway

    def valuate_content_of_f(self, tuples):

        args = [i[0] for i in tuples]

        if '-graph' in args:
            # pos = args.index('-graph')  # do something to it
            if tuples[0][1] not in ['normal', 'botnet']:
                sys.exit("[!] WARNING: a -graph devi passare come argomento 'normal' o 'botnet'.")

        if '-l' in args:
            pos = args.index('-l')  # do something to it
            if tuples[pos][1] not in ['normal', 'botnet']:
                sys.exit("[!] WARNING: a -graph devi passare come argomento 'normal' o 'botnet'.")

            # non faccio controlli sull'esistenza dei file .csv, poichè se non ci sono devono essere creati.
            # nella documentazione devo scrivere che l'eventuale directory dei file esistenti deve essere: temporary/

        # print("len(tuples): %d" % len(tuples))
        #
        # for t in range(0, len(args)):
        #     print(tuples[t][0])
        #
        #     if arg is '-graph':
        #         if tuples[t][1] is not ('normal', 'botnet'):
        #             sys.exit("[!] WARNING: a -graph devi passare come argomento 'legitimate' o 'botnet'.")
        #
        #     if arg

        # if tuples[t][0] is '-graph':
        #     if tuples[t][1] is not ('normal', 'botnet'):
        #         sys.exit("[!] WARNING: a -graph devi passare come argomento 'legitimate' o 'botnet'")
        # if tuples[t][0] is '-l' or tuples[t][1] is '-l':
        #     if tuples[t][0] is not ('normal', 'botnet') or tuples[t][1] is not ('normal', 'botnet'):
        #         sys.exit("[!] WARNING: a -graph devi passare come argomento 'legitimate' o 'botnet'")
        # len(tuples)
        # if tuples[t][1] is '-s' or tuples[t][2] is '-s':  # TODO: non so come farla
        #     if tuples[t][1] is not ('normal', 'botnet') or tuples[t][2] is not ('normal', 'botnet'):
        #         sys.exit("[!] WARNING: a -graph devi passare come argomento 'legitimate' o 'botnet'")

        return True

    def execute(self):

        tuples_args = self.convert_arguments_in_tuples()

        self.valuate_existence_functionality(tuples_args)

        self.valuate_length_of_tuple(tuples_args)

        self.valuate_content_of_tuple(tuples_args)

        return tuples_args
