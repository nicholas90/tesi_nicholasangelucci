import datetime

import dpkt

import Utility_PCAP


class ParsePcap(object):

    def __init__(self, directory_file_pcap):

        self.directory_file_pcap = directory_file_pcap
        # self.name_filePCAP = self.getName_filePCAP
        self.number_packets = self.calculate_number_packets()
        self.prova = self.prova()

    def getName_filePCAP(self):

        return self.directory_file_pcap


    def prova(self):

        filePCAP = self.directory_file_pcap
        timestamp_inizio = 0

        # legge ogni pacchetto contenuto nel file .pcap
        with open(filePCAP, 'rb') as f:

            pcap = dpkt.pcap.Reader(f)

            p = 0

            # For each packet in the pcap process the contents
            for timestamp, buf in pcap:
                if p == 0:
                    timestamp_inizio = datetime.datetime.fromtimestamp(timestamp)
                    break

        return timestamp_inizio

    # calcola il numero di pacchetti totale del file pcap
    def calculate_number_packets(self):

        filePCAP = self.directory_file_pcap

        try:

            with open(filePCAP, 'rb') as f:

                pcap = dpkt.pcap.Reader(f)

                return len(list(pcap))

        except IsADirectoryError as e:

            print(e)


    # stampa informazioni inerenti un pacchetto, poi le sposto in base a qual è la features da analizzare
    def print_information_packet(self, buf):

        ''' stampa informazioni ottenute al Layer 2 del modello iso/osi '''

        # Unpack the Ethernet frame (mac src/dst, ethertype)
        eth = dpkt.ethernet.Ethernet(buf)
        print('Ethernet Frame: ', Utility_PCAP.mac_addr(eth.src), Utility_PCAP.mac_addr(eth.dst), eth.type, '\n')

        ''' stampa informazioni ottenute al Layer 3 del modello iso/osi '''

        ip = eth.data

        # Print out the info
        print('IP: %s -> %s   (len=%d ttl=%d) \n' %
              (Utility_PCAP.inet_to_str(ip.src), Utility_PCAP.inet_to_str(ip.dst), ip.len, ip.ttl))

        ''' stampa informazioni ottenute al Layer 4 del modello iso/osi '''

        tcp = ip.data

        fin_flag = (tcp.flags & dpkt.tcp.TH_FIN) != 0
        syn_flag = (tcp.flags & dpkt.tcp.TH_SYN) != 0
        rst_flag = (tcp.flags & dpkt.tcp.TH_RST) != 0
        psh_flag = (tcp.flags & dpkt.tcp.TH_PUSH) != 0
        ack_flag = (tcp.flags & dpkt.tcp.TH_ACK) != 0
        urg_flag = (tcp.flags & dpkt.tcp.TH_URG) != 0
        ece_flag = (tcp.flags & dpkt.tcp.TH_ECE) != 0
        cwr_flag = (tcp.flags & dpkt.tcp.TH_CWR) != 0

        print(
            'TCP flag set: fin_flag=%d, syn_flag=%d, rst_flag=%d, psh_flag=%d, ack_flag=%d, urg_flag=%d, ece_flag=%d, '
            'cwr_flag=%d \n' % (fin_flag, syn_flag, rst_flag, psh_flag, ack_flag, urg_flag, ece_flag, cwr_flag))
