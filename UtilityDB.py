import os

import sqlite3
from sqlite3 import Error


# from sqlite3 import dbapi2 as sqlite
#
#
# def unlock_db(db_filename):
#     """Replace db_filename with the name of the SQLite database."""
#     connection = sqlite.connect(db_filename)
#     connection.commit()
#     connection.close()


def get_database():
    """
    Restituisce il nome del database tutilizzato nel progetto

    :return: string conenente il nome del database utilizzato nel progetto
    """

    return "database.db"


def remove_database():
    database = get_database()

    os.remove(database)


def attach_database(conn):
    """
    Seleziona un determinato database.

    Dopo aver chiamato questa funzione tutti gli statements SQLite verranno eseguiti sul database assegnato
    (quindi sull'attached database).

    :param conn: l'oggetto Connection, che rappresenta il Database
    :return:
    """

    # Crea un oggetto Connection il quale rappresenta il database.
    # Una volta creato l'oggetto, i dati saranno memorizzati nel file chiamato 'database.db'.
    sqlite3.connect('database.db')

    # Obtain a cursor object
    c = conn.cursor()

    attach = "ATTACH DATABASE ? AS database"

    db = ('database.db',)

    c.execute(attach, db)


def create_table():
    """
    Crea la tabella 'preprocessed_file', se non esiste

    :return: True in caso di successo, altrimenti genera eccezione.
    """

    # Create table
    query = """CREATE TABLE IF NOT EXISTS preprocessed_file(idfile INTEGER PRIMARY KEY, 
                      name_file VARCHAR(255) NOT NULL, tw INT(6) NOT NULL)"""

    conn = sqlite3.connect('database.db')

    c = conn.cursor()

    try:
        c.execute(query)

    except Error as e:
        print("""Error occur: %s""" % e)

    finally:
        if conn:
            conn.close()

        return True


def create_table_dataset_composition():
    """
    Crea la tabella 'dataset_composition', se non esiste

    :return: True in caso di successo, altrimenti genera eccezione.
    """

    # Create table
    query = """CREATE TABLE IF NOT EXISTS dataset_composition(idfile INTEGER PRIMARY KEY, 
                      name_file VARCHAR(255) NOT NULL, number_tw INT(12) NOT NULL, label INT(1) NOT NULL)"""

    conn = sqlite3.connect('database.db')

    c = conn.cursor()

    try:
        c.execute(query)

    except Error as e:
        print("""Error occur: %s""" % e)

    finally:
        if conn:
            conn.close()

        return True


def execute_insert_tuple(nome_file, tw):
    t = (nome_file, tw,)
    query = """INSERT INTO preprocessed_file ('name_file', 'tw') VALUES (?, ?)"""

    conn = sqlite3.connect('database.db')
    c = conn.cursor()

    try:

        c.execute(query, t)

        # Save (commit) the changes
        conn.commit()

    except sqlite3.IntegrityError as e:
        print("""Error occured: %s""" % e)
    except Exception as e:
        print("""Exception: %s, Error: %s""" % (query, str(e)))

    finally:
        if conn:
            conn.close()

        return True


def execute_insert_dataset_composition(nome_file, number_tw, label):
    t = (nome_file, number_tw, label,)
    query = """INSERT INTO dataset_composition ('name_file', 'number_tw', 'label') VALUES (?, ?, ?)"""

    conn = sqlite3.connect('database.db')
    c = conn.cursor()

    try:

        c.execute(query, t)

        # Save (commit) the changes
        conn.commit()

    except sqlite3.IntegrityError as e:
        print("""Error occured: %s""" % e)
    except Exception as e:
        print("""Exception: %s, Error: %s""" % (query, str(e)))

    finally:
        if conn:
            conn.close()

        return True


def execute_select_tuple(nome_file, tw):
    t = (nome_file, tw,)
    query = """SELECT * FROM preprocessed_file WHERE name_file=? AND tw=?"""

    conn = sqlite3.connect('database.db')
    c = conn.cursor()

    try:

        c.execute(query, t)

        # Save (commit) the changes
        conn.commit()

        entry = c.fetchone()

        if entry is not None:
            return True
        else:
            return False

    except sqlite3.Error as e:
        print("""Database error on query: '%s', Error: %s""" % (query, e))
    except Exception as e:
        print("""Exception Query Failed: %s, Error: %s""" % (query, str(e)))

    finally:
        if conn:
            conn.close()


def execute_select_dataset_composition(nome_file):
    t = (nome_file,)
    query = """SELECT * FROM dataset_composition WHERE name_file=?"""

    conn = sqlite3.connect('database.db')
    c = conn.cursor()

    try:

        c.execute(query, t)

        # Save (commit) the changes
        conn.commit()

        entry = c.fetchone()

        if entry is not None:
            return True
        else:
            return False

    except sqlite3.Error as e:
        print("""Database error on query: '%s', Error: %s""" % (query, e))
    except Exception as e:
        print("""Exception Query Failed: %s, Error: %s""" % (query, str(e)))

    finally:
        if conn:
            conn.close()


def execute_delete_tuple(nome_file, tw):
    t = (nome_file, tw,)
    query = """DELETE FROM preprocessed_file WHERE name_file=? AND tw=?"""

    conn = sqlite3.connect('database.db')
    c = conn.cursor()

    try:

        c.execute(query, t)

        # Save (commit) the changes
        conn.commit()

    except sqlite3.Error as e:
        print("""Error occur: %s""" % e)
        return False
    except Exception as e:
        print("""Exception: %s, Error: %s""" % (query, str(e)))
        return False

    finally:
        if conn:
            conn.close()

        return True
