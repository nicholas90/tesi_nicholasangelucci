import shutil

import dpkt

import Run_Program
from ParsePcap import ParsePcap


class Split_in_flow(ParsePcap):

    #TODO: chiedere domani
    def __init__(self, directory_file_pcap):

        super(Split_in_flow, self).__init__(directory_file_pcap)

    # controlla se nella cartella ci sono le directory: 'bidirectional-flows' e 'discarded-flow', altrimenti le crea
    def check_directory(self):

        print('check_directory: %s' % self.directory_file_pcap)

    # splitta il file pcap in flussi bidirezionali, ossia: comunicazioni bidirezionali tra due host
    def split_in_bidirectional_flows(self, path_bidirectional_flows):

        # print('directory_filePCAP:', self.directory_filePCAP)

        # print('path_bidirectional_flows:', path_bidirectional_flows)

        Run_Program.run_splitcap_splitInBidirectionalFlows(self.directory_file_pcap, path_bidirectional_flows)

    # splitta il file pcap in TCP section
    def split_in_5tupla(self, path_5tupla):

        # print('directory_filePCAP:', self.directory_filePCAP)

        # print('path_tcp_session:', path_5tupla)

        Run_Program.run_splitcap_splitInSession(self.directory_file_pcap, path_5tupla)

    # splitta il file pcap in TCP section (con il programma PcapSplitter)
    def split_in_5tuple_splitter(self, path_5tupla):

        Run_Program.run_pcapplitter_split5tupla(path_file_pcap=self.directory_file_pcap, path_5tuple=path_5tupla)

    # splitta il file pcap in TCP section
    def split_in_tcp_session(self, path_tcp_session):

        # print('directory_filePCAP:', self.directory_filePCAP)

        # print('path_tcp_session:', path_tcp_session)

        Run_Program.run_splitcap_splitInSession(self.directory_file_pcap, path_tcp_session)


    # TODO: IMPORTANTE, CAPIRE SE TUTTE LE FEATURES HANNO BISOGNO DELLA VALIDAZIONE: <=> http

    def filter_tcp(self, path_flows, path_discarded_flows):

        """
        Verifica se il file .pcap è flusso TCP.
        Se lo è, rimane in nella directory dove si trova, ossia 'path_flows';
        altrimenti viene spostato nella directory 'path_discarded_flows'.

        # e se trasporta traffico HTTP.

        :param path_flows: path contente i flussi.
        :param path_discarded_flows: path nel quale verranno spostati i flussi non TCP.
        :return: true
        """

        file = self.directory_file_pcap

        # is_present_request = False
        is_tcp_flow = False

        with open(file, 'rb') as f:

            pcap = dpkt.pcap.Reader(f)

            # For each packet in the pcap process the contents
            for timestamp, buf in pcap:

                # Unpack the Ethernet frame (mac src/dst, ethertype)
                eth = dpkt.ethernet.Ethernet(buf)

                # Make sure the Ethernet data contains an IP packet
                if not isinstance(eth.data, dpkt.ip.IP):
                    continue

                # Now grab the data within the Ethernet frame (the IP packet)
                ip = eth.data

                # Check for TCP in the transport layer
                if not isinstance(ip.data, dpkt.tcp.TCP):
                    continue

                is_tcp_flow = True

                # Set the TCP data
                # tcp = ip.data

                # Now see if we can parse the contents as a HTTP request
                # try:
                #     dpkt.http.Request(tcp.data)
                #     # http = dpkt.http.Request(tcp.data)
                # except (dpkt.dpkt.NeedData, dpkt.dpkt.UnpackError):
                #     continue

                # print('request.data: %s' % request.data)

                # praticamente, se genera eccezione,
                # si riparte dall'if senza settare il booleano (is_present_request) a True
                # is_present_request = True

        f.close()

        if not is_tcp_flow:
            shutil.move(file, path_discarded_flows)

# -------------------------------------------------------------------------------------

        # non ci va:
                #else:

                    #shutil.move("path/to/current/file.foo", "path/to/new/destination/for/file.foo")

        # for p in packets:
        #     print('asd: ', p.haslayer('HTTP'))
        #from scapy.all import IP, sniff
        #from scapy.layers import http

         #from scapy import *

        # '''
        # Processes a TCP packet, and if it contains an HTTP request, it prints it.
        # '''
        # if not packet.haslayer(http.HTTPRequest):
        #     # This packet doesn't contain an HTTP request so we skip it
        #     return
        # http_layer = packet.getlayer(http.HTTPRequest)
        # ip_layer = packet.getlayer(IP)
        # print
        # '\n{0[src]} just requested a {1[Method]} {1[Host]}{1[Path]}'.format(ip_layer.fields, http_layer.fields)



        # # Open the pcap file
        # f = open(file, 'rb')
        # pcap = dpkt.pcap.Reader(f)
        #
        # # I need to reassmble the TCP flows before decoding the HTTP
        # conn = dict()  # Connections with current buffer
        # for ts, buf in pcap:
        #     eth = dpkt.ethernet.Ethernet(buf)
        #     if eth.type != dpkt.ethernet.ETH_TYPE_IP:
        #         continue
        #
        #     ip = eth.data
        #     if ip.p != dpkt.ip.IP_PROTO_TCP:
        #         continue
        #
        #     tcp = ip.data
        #
        #     if tcp.dport == 80 and len(tcp.data) > 0:
        #         try:
        #             http = dpkt.http.Request(tcp.data)
        #             print(http.uri)
        #         except:
        #             print('issue')
        #             continue
        #
        # f.close()

            # tupl = (ip.src, ip.dst, tcp.sport, tcp.dport)
            # # print tupl, tcp_flags(tcp.flags)
            #
            # syn_flag = (tcp.flags & dpkt.tcp.TH_SYN) != 0
            #
            # fin_flag = (tcp.flags & dpkt.tcp.TH_FIN) != 0
            # ack_flag = (tcp.flags & dpkt.tcp.TH_ACK) != 0
            #
            # import Utility_PCAP
            #
            # print('syn_flag: %s && (fin_flag %s ack_flag %s)' % (syn_flag, fin_flag, ack_flag))
            #
            # print(Utility_PCAP.inet_to_str(ip.dst))
            #
            # if (not syn_flag || not fin_flag || not ack_flag):
            #
            #    print('syn_flag: %s && (fin_flag %s ack_flag %s)' % (syn_flag, fin_flag, ack_flag))
            #
            # if Utility_PCAP.inet_to_str(ip.dst) == '74.125.232.195':
            #     print('----------------------------------------------asfdfdasd')
            #     print('tcp')
            #
            # tupl = (ip.src, ip.dst, tcp.sport, tcp.dport)
            # # print tupl, tcp_flags(tcp.flags)
            #
            # # Ensure these are in order! TODO change to a defaultdict
            # if tupl in conn:
            #     conn[tupl] = conn[tupl] + tcp.data
            # else:
            #     conn[tupl] = tcp.data
            #
            # # Try and parse what we have
            # try:
            #     stream = conn[tupl]
            #     if stream[:4] == 'HTTP':
            #         http = dpkt.http.Response(stream)
            #         # print http.status
            #     else:
            #         http = dpkt.http.Request(stream)
            #         # print http.method, http.uri
            #
            #     print('http:', http)
            #
            #     # If we reached this part an exception hasn't been thrown
            #     stream = stream[len(http):]
            #     if len(stream) == 0:
            #         del conn[tupl]
            #     else:
            #         conn[tupl] = stream
            # except dpkt.UnpackError:
            #     pass

            # if tcp.dport == 80 and len(tcp.data) > 0:
            #     http = dpkt.http.Request(tcp.data)
            #     print('------------------importante')
            #
            # if tcp.dport == 80 and len(tcp.data) > 0:
            #     http = dpkt.http.Request(tcp.data)


