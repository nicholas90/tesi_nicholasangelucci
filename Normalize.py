import csv

import pandas as pd
from sklearn import preprocessing

import Utility.UtilityNormalize as UtilityNormalize

"""carica il file csv in un numpy array"""

"""normalizzo"""

"""riaggiungo la colonna con le label"""


def get_features(file_csv):
    import numpy as np

    all_features = []

    # per ogni colonna (tranne l'ultima)
    for i in range(0, 8):

        features = []

        # open csv file
        with open(file_csv) as file:

            read_csv = csv.reader(file, delimiter=',')

            for row in read_csv:
                v = row[i]

                features.append(v)

        features = np.asarray(features)

        all_features.append(features)

    print("---------------")

    all_features = np.vstack(all_features)

    return all_features


def get_normalized_dataframe(file_csv):
    # all_features = validation.as_float_array(all_features, copy=True)

    # normalized = preprocessing.MinMaxScaler(feature_range=(0, 1))
    #
    # normalized_dataset = normalized.fit_transform(all_features)

    path_temporary_csv = UtilityNormalize.get_directory_temporary() + file_csv

    temporary_csv = pd.read_csv(path_temporary_csv, header=None)

    x = temporary_csv.values  # returns a numpy array
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(x)
    df = pd.DataFrame(x_scaled)

    # print(df)

    return df

    exit(0)
    return df

    exit(0)

    min_max_scaler = preprocessing.MinMaxScaler(feature_range=(0, 1), copy=True)
    x_scaled = min_max_scaler.fit_transform(all_features)
    df = pd.DataFrame(x_scaled)

    print(df)
    exit(0)

    print(normalized_dataset)

    return normalized_dataset


def get_labelled_dataframe(file_csv):
    path_temporary_csv = UtilityNormalize.get_directory_temporary() + file_csv

    temporary_csv = pd.read_csv(path_temporary_csv, header=None)

    x = temporary_csv.values  # returns a numpy array
    df = pd.DataFrame(x)

    # print(df)

    return df


def write_csv(dataframe):
    prova = UtilityNormalize.get_directory_dataset() + "dataset.csv"

    dataframe.to_csv(path_or_buf=prova, header=False, index=False)

    return True


def get_label(file_csv):
    label = 9

    with open(file_csv) as file:
        read_csv = csv.reader(file, delimiter=',')

        for row in read_csv:
            label = row[8]
            break

    return label


def get_normalized_labelled_dataset(normalized_df, labelled_df):
    result_df = pd.concat([normalized_df, labelled_df], axis=1, ignore_index=True)

    return result_df

# def add_label_to_normalize_dataset(normalized_dataset, label):
#
#     minmax_normalized_labelled = np.insert(normalized_dataset, 8, label, axis=1)
#
#     return minmax_normalized_labelled
