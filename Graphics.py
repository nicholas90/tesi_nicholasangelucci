import matplotlib.pyplot as plt
import numpy as np

import Utility.Utils as Utils


def get_graph(path, lista, number_features, name_features, xlabel, ylabel, color=False):
    # lista = dict.tolist()

    plt.title(name_features)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    if not color:
        plt.stem(lista)
    elif color == 'normal':
        plt.stem(lista, 'g-o')
    elif color == 'botnet':
        plt.stem(lista, 'r-o')

    figura = path + '/'

    Utils.check_directory(figura)

    figura = figura + number_features + '.png'

    plt.savefig(fname=figura, dpi='figure', format='png')

    plt.show()


def get_histogram(feature_number, bins_number, value):
    """


    :param feature_number:
    :param bins_number:
    :param value:
    :return:
    """
    categorie = []

    for c in range(1, bins_number):
        categorie.append(c)

    x = np.arange(len(categorie))

    fig, ax = plt.subplots()

    ax.set_xlabel('Bins')
    ax.set_ylabel('Numero di campioni')
    ax.set_title("Features %d - %d bins" % (feature_number, bins_number - 1))
    plt.bar(x, value)
    plt.xticks(x, categorie)

    nome_png = "Features%d-%dbins" % (feature_number, bins_number - 1)

    figura = get_graphics_path() + nome_png + ".png"

    plt.savefig(fname=figura, dpi='figure', format='png')
    plt.show()


def get_graphics_path():
    return "graphics/"
