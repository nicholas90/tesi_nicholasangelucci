November 2017 release of PcapPlusPlus (v17.11)
==============================================

PcapPlusPlus web-site:
----------------------
http://seladb.github.io/PcapPlusPlus-Doc/

GitHub page: https://github.com/seladb/PcapPlusPlus


This package contains:
----------------------

 - PcapPlusPlus compiled libraries
    - libCommon++.a
    - libPacket++.a
    - libPcap++.a
 - PcapPlusPlus header files (under header/)
 - Compiled examples (under examples/)
 - Installation scripts (install.sh & uninstall.sh)
 - Code example with a simple Makefile showing how to build applications using PcapPlusPlus compiled binaries (under example-app/)


Installation:
-------------

 - For installing the package simply run: sudo ./install.sh


Running the examples:
---------------------

 - Make sure you have libpcap installed (should come built-in with most Linux and Mac OS X distributions)
 - In Linux you may need to run the executables as sudo


In order to compile your application with these binaries you need to:
---------------------------------------------------------------------

 - Make sure you have libpcap developer pack installed (in Ubuntu: install it using apt-get: "sudo apt-get install libpcap-dev"; in Mac OS X it should come with Xcode)
 - Make sure the binaries matches your OS (for example: pcapplusplus-17.11-ubuntu-16.04-x86-64-gcc-4.8.tar.gz pack probably won't necessarily well in Ubuntu 14.04)
 - Make sure your gcc/g++ version matches the compiled binaries version (for example: pcapplusplus-17.11-ubuntu-16.04-x86-64-gcc-4.8.tar.gz pack probably won't work well with gcc 4.9)
 - Make sure PcapPlusPlus is installed (see Installation section above)
 - Include /usr/local/etc/PcapPlusPlus.mk in your Makefile and add PcapPlusPlus includes and libs in relevant place in your Makefile 
     - You can follow the example under example-app/


Release notes (changes from v17.02):
------------------------------------

 - Added TCP reassembly module
 - Set Unilicense as PcapPlusPlus license
 - Added support for DPDK 16.11 and 17.02 (replacing old 2.1 version)
 - Added 5 tutorials for getting started with PcapPlusPlus: http://seladb.github.io/PcapPlusPlus-Doc/tutorials.html
 - Added installation target ('make install') on Linux and MacOS
 - New protocols:
   - SIP
   - SDP
   - IPv4 options
   - Raw IP link layer type
   - VXLAN
 - New and updated utilities:
   - New TCP reassembly utility
   - PcapSplitter now includes split criteria in split file names (for example: when splitting by client IP output files will look like: filename-client-ip-x.x.x.x.pcap). This applies also to splitting by server IP, server port and BPF filter (thanks to @bpagon13 !)
   - PcapSplitter now supports splitting pcap files with link type other than Ethernet
   - Added version information for all utilities
 - Choose whether to parse a packet fully or up to a certain layer
 - Added support for libpcap immediate mode where supported (libpcap ver>=1.5) - thanks to @f-squirrel !
 - pthreads on MinGW are now linked statically (upgraded pthreads to 2.10)
 - Added PcapPlusPlus version and Git info API
 - Clean-ups:
   - Removed unnecessary files and reduced repo size
   - Removed compilation warnings
   - Cleaner makefiles and makefile output
   - Moved debug-new to 3rd-party
 - Many bugfixes

