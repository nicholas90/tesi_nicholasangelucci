import matplotlib.pyplot as plt
import numpy as np
from sklearn.ensemble import ExtraTreesClassifier

dataset = np.loadtxt(fname="dataset/dataset.csv", delimiter=",")

X = dataset[:, 0:8].astype(float)
Y = dataset[:, 8]

forest = ExtraTreesClassifier(n_estimators=250,
                              random_state=0,
                              n_jobs=-1)

forest = forest.fit(X, Y)

# print(clf.feature_importances_)
importances = forest.feature_importances_

std = np.std([tree.feature_importances_ for tree in forest.estimators_], axis=0)

indices = np.argsort(importances)[::-1]

# Print the feature ranking
print("Feature ranking:")

for f in range(X.shape[1]):
    print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))

# Plot the feature importances of the forest
plt.figure()
plt.title("Importanza delle caratteristiche")
plt.bar(range(X.shape[1]), importances[indices],
        color="r", yerr=std[indices], align="center")
plt.xticks(range(X.shape[1]), indices)
plt.xlim([-1, X.shape[1]])
plt.show()
